Geon
====

Geon is a 3D model viewer for the GNOME desktop.

----

Geon is built using meson:

```sh
meson build --prefix=/usr
ninja -C build
sudo ninja -C build install
```

----

Geon is licensed under the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.
