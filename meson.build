project(
  'geon', 'c',
  version: '3.28.0',
  meson_version: '>= 0.50.1',
  license: 'GPL3',
  default_options: [
    'buildtype=debugoptimized',
    'warning_level=1',
    'c_std=gnu99',
  ],
)

gnome = import('gnome')
i18n = import('i18n')

# Versioning
api_version = '1.0'
lib_version = '0.0.0'
version = meson.project_version()

package_name = meson.project_name()
package_string = '@0@-@1@'.format(package_name, api_version)

# Paths
rootdir = include_directories('.')

srcdir = meson.source_root()
builddir = meson.build_root()

prefix = get_option('prefix')

bindir = join_paths(prefix, get_option('bindir'))
libdir = join_paths(prefix, get_option('libdir'))
includedir = join_paths(prefix, get_option('includedir'))
datadir = join_paths(prefix, get_option('datadir'))
localedir = join_paths(prefix, get_option('localedir'))

dbusdir = join_paths(datadir, 'dbus-1')
girdir = join_paths(datadir, 'gir-1.0')
typelibdir = join_paths(libdir, 'girepository-1.0')
vapidir = join_paths(datadir, 'vala', 'vapi')

pkgincludedir = join_paths(includedir, package_string)

# Dependencies
cc = meson.get_compiler('c')

glib_req = '>= 2.56.0'
wayland_req = '1.14.0'
graphene_req = '>= 1.8.0'
gtk_req = '>= 4.0.0'
libadwaita_req = '>= 1.0.0'
peas_req = '>= 1.22.0'

epoxy_req = '>= 1.4.0'
gthree_req = '>= 0.2.0'
vulkan_req = '>= 1.1.0'

glib_dep = dependency('glib-2.0', version: glib_req)
gobject_dep = dependency('gobject-2.0', version: glib_req)
gio_dep = dependency('gio-2.0', version: glib_req)
peas_dep = dependency('libpeas-1.0', version: peas_req)
graphene_dep = dependency('graphene-1.0', version: graphene_req)
wayland_dep = dependency('wayland-client')
gtk_dep = dependency('gtk4', version: gtk_req)
libadwaita_dep = dependency('libadwaita-1', version: libadwaita_req)

epoxy_dep = dependency(
  'epoxy',
  required: get_option('opengl'),
  version: epoxy_req)
gthree_dep = dependency(
  'gthree-1.0',
  required: get_option('opengl'),
  version: gthree_req,
  fallback: [
    'gthree',
    'libgthree_gtk4_dep'],
  default_options: [
    'shared_lib=true',
    'gtk3=false',
    'gtk4=true',
  ],
)

vulkan_dep = dependency(
  'vulkan',
  required: get_option('vulkan'),
  version: vulkan_req,
)

gtk_is_subproject = false
if gtk_dep.type_name() == 'internal'
  gtk_is_subproject = true
  gtk_subproject_typelib_dir = join_paths(
    meson.build_root(),
    'subprojects',
    'gtk', 'gtk')
endif

gthree_is_subproject = false
if gthree_dep.type_name() == 'internal'
  gthree_is_subproject = true
  gthree_subproject_typelib_dir = join_paths(
    meson.build_root(),
    'subprojects',
    'gthree', 'gthree')
endif

# Options
enable_opengl = gthree_dep.found()
enable_vulkan = vulkan_dep.found()

generate_gir = get_option('introspection')
generate_vapi = get_option('vapi')
if generate_vapi and not generate_gir
  generate_vapi = false
endif

# Configuration
add_project_arguments([
  '-DHAVE_CONFIG_H',
  '-I' + meson.build_root(),
  '-D_GNU_SOURCE'
], language: 'c')

if not get_option('buildtype').startswith('debug')
  add_project_arguments([
    '-DG_DISABLE_CAST_CHECKS',
    '-DG_DISABLE_ASSERT',
  ], language: 'c')
else
  add_project_arguments([
    '-DGEON_ENABLE_TRACE',
  ], language: 'c')
endif

if enable_opengl == true
  add_project_arguments([
    '-DGEON_RENDERING_OPENGL',
  ], language: 'c')
endif

if enable_vulkan == true
  add_project_arguments([
    '-DGEON_RENDERING_VULKAN',
  ], language: 'c')
endif

# Mute unused warnings for now...
add_project_arguments([
  '-Wno-unused-function',
  '-Wno-unused-but-set-variable',
  ], language: 'c')

config_h = configuration_data()
config_h.set_quoted('PACKAGE_NAME', package_name)
config_h.set_quoted('PACKAGE_VERSION', version)
config_h.set_quoted('PACKAGE_API_VERSION', api_version)
config_h.set_quoted('PACKAGE_STRING', package_string)
config_h.set_quoted('PACKAGE_LIBDIR', libdir)
config_h.set_quoted('PACKAGE_DATADIR', datadir)
config_h.set_quoted('PACKAGE_LOCALEDIR', localedir)

config_h.set_quoted('GEON_APPLICATION_ID', 'org.gnome.Geon')
config_h.set_quoted('GEON_RESOURCE_BASE_PATH', '/org/gnome/geon')

config_h.set('VERSION', 'PACKAGE_VERSION')
config_h.set('GETTEXT_PACKAGE', 'PACKAGE_NAME')
config_h.set('LOCALEDIR', 'PACKAGE_LOCALEDIR')

configure_file(
  output: 'config.h',
  configuration: config_h,
)

subdir('data')
subdir('src')
subdir('po')

meson.add_install_script('build-aux/meson/post_install.py')

summary = [
  '',
  '------',
  'Geon @0@ (@1@)'.format(version, api_version),
  '',
  '  OpenGL support: @0@'.format(enable_opengl),
  '  Vulkan support: @0@'.format(enable_vulkan),
  '   Introspection: @0@'.format(generate_gir),
  '        Vala API: @0@'.format(generate_vapi),
  '',
  'Directories:',
  '',
  '          prefix: @0@'.format(prefix),
  '      includedir: @0@'.format(includedir),
  '          libdir: @0@'.format(libdir),
  '         datadir: @0@'.format(datadir),
  '------',
]

message('\n'.join(summary))
