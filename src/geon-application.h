/* This file is part of Geon: geon-application.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

#include <adwaita.h>

G_BEGIN_DECLS

#define GEON_TYPE_APPLICATION (geon_application_get_type())

G_DECLARE_FINAL_TYPE (GeonApplication, geon_application,
                      GEON, APPLICATION,
                      AdwApplication)

GeonApplication   *geon_application_new               (void);

void               geon_application_open_uri          (GeonApplication *self,
                                                       const gchar     *file_uri);

void               geon_application_open_file         (GeonApplication *self,
                                                       GFile           *file);

void               geon_application_set_accelerator   (GeonApplication       *self,
                                                       const gchar           *action_name,
                                                       const guint            accelerator_key,
                                                       const GdkModifierType  accelerator_mods);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
