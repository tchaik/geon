/* This file is part of Geon: geon-vk-area.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-VKArea"

#include "config.h"

#include <glib/gi18n.h>
#include <glib-object.h>
#include <vulkan/vulkan.h>

#include "geon-debug.h"

#include "geon-vk-area.h"

typedef struct
{
} GeonVKAreaPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (GeonVKArea, geon_vk_area,
                            GTK_TYPE_WIDGET)

static void
geon_vk_area_resize (GeonVKArea *self,
                     gint        width,
                     gint        height)
{
  GeonVKAreaPrivate *priv;

  g_return_if_fail (GEON_IS_VK_AREA (self));

  priv = geon_vk_area_get_instance_private (self);
}

static GdkVulkanContext *
geon_vk_area_create_context (GeonVKArea *self)
{
  GeonVKAreaPrivate *priv;

  g_return_val_if_fail (GEON_IS_VK_AREA (self), NULL);

  priv = geon_vk_area_get_instance_private (self);

  return NULL;
}

static void
geon_vk_area_finalize (GObject *object)
{
  GeonVKArea *self = GEON_VK_AREA (object);
  GeonVKAreaPrivate *priv;

  g_return_if_fail (GEON_IS_VK_AREA (self));

  priv = geon_vk_area_get_instance_private (self);

  G_OBJECT_CLASS (geon_vk_area_parent_class)->finalize (object);
}

static void
geon_vk_area_class_init (GeonVKAreaClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = geon_vk_area_finalize;

  klass->resize = geon_vk_area_resize;
  klass->create_context = geon_vk_area_create_context;
}

static void
geon_vk_area_init (GeonVKArea *self)
{
}

GtkWidget *
geon_vk_area_new ()
{
  return g_object_new (GEON_TYPE_VK_AREA, NULL);
}

/* ex:set ts=2 sw=2 et: */
