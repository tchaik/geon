/* This file is part of Geon: geon-vk-utils.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-VKUtils"

#include "config.h"

#include <inttypes.h>

#include <glib/gi18n.h>
#include <vulkan/vulkan.h>

#include "geon-debug.h"

#include "geon-vk-utils.h"

const gchar *
device_type_to_string (VkPhysicalDeviceType device_type)
{
  switch (device_type)
    {
    case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
      return "integrated GPU";
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
      return "discreate GPU";
    case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
      return "virtual GPU";
    case VK_PHYSICAL_DEVICE_TYPE_CPU:
      return "CPU";
    case VK_PHYSICAL_DEVICE_TYPE_OTHER:
      return "other";
    default:
      return "unknown";
    }
}

uint32_t
make_version (const gchar *version)
{
  gchar *needle = (gchar *)version;
  guint64 versions[3] = { 0, 0, 0 };
  gint i;

  g_return_val_if_fail (version != NULL, 0);

  for (i = 0; i < 3; i++)
    {
      if (needle != NULL)
        {
          versions[i] = g_ascii_strtoull (needle, &needle, 10);
          needle += sizeof (gchar);
        }
    }

  return VK_MAKE_VERSION (versions[0], versions[1], versions[2]);
}

void
geon_vk_utils_print_info (void)
{
  PFN_vkEnumerateInstanceVersion vkEnumerateInstanceVersion;
  VkInstance instance;
  uint32_t version = VK_API_VERSION_1_0;
  uint32_t major, minor, patch;
  uint32_t device_count = 0;
  VkPhysicalDevice *devices;
  VkResult result;
  guint i, j;

  g_print ("* Vulkan (vulkan):\n");

  vkEnumerateInstanceVersion = (PFN_vkEnumerateInstanceVersion)
    vkGetInstanceProcAddr (NULL, "vkEnumerateInstanceVersion");

  if (vkEnumerateInstanceVersion != NULL)
    result = vkEnumerateInstanceVersion (&version);

  major = VK_VERSION_MAJOR (version);
  minor = VK_VERSION_MINOR (version);
  patch = VK_VERSION_PATCH (VK_HEADER_VERSION);

  g_print ("   API version: %d.%d.%d\n", major, minor, patch);

  VkApplicationInfo application_info = {
    .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
    .pNext = NULL,
    .pApplicationName = _("Geon"),
    .applicationVersion = make_version (PACKAGE_VERSION),
    .pEngineName = PACKAGE_NAME,
    .engineVersion = make_version (PACKAGE_API_VERSION),
    .apiVersion = version
  };

  VkInstanceCreateInfo instance_info = {
    .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
    .pNext = NULL,
    .flags = 0,
    .pApplicationInfo = &application_info,
    .enabledLayerCount = 0,
    .ppEnabledLayerNames = NULL,
    .enabledExtensionCount = 0,
    .ppEnabledExtensionNames = NULL
  };

  result = vkCreateInstance (&instance_info, NULL, &instance);

  result = vkEnumeratePhysicalDevices (instance, &device_count, NULL);
  devices = g_new (VkPhysicalDevice, device_count);
  result = vkEnumeratePhysicalDevices (instance, &device_count, devices);

  for (i = 0; i < device_count; i++)
    {
      VkPhysicalDeviceProperties device_properties;
      VkPhysicalDeviceFeatures device_features;
      VkQueueFamilyProperties *queue_family_properties;
      uint32_t queue_family_count = 0;

      vkGetPhysicalDeviceProperties (devices[i], &device_properties);
      vkGetPhysicalDeviceFeatures (devices[i], &device_features);

      vkGetPhysicalDeviceQueueFamilyProperties (devices[i],
                                                &queue_family_count,
                                                NULL);

      queue_family_properties = g_new (VkQueueFamilyProperties,
                                       queue_family_count);

      vkGetPhysicalDeviceQueueFamilyProperties (devices[i],
                                                &queue_family_count,
                                                queue_family_properties);

      g_print ("   Device #%u (%s):\n", i,
               device_type_to_string (device_properties.deviceType));
      g_print ("     Name: %s\n", device_properties.deviceName);
      g_print ("     Vendor ID: 0x%Xu\n", device_properties.vendorID);
      g_print ("     Device ID: 0x%Xu\n", device_properties.deviceID);
      g_print ("     API version: %u.%u.%u\n",
               VK_VERSION_MAJOR (device_properties.apiVersion),
               VK_VERSION_MINOR (device_properties.apiVersion),
               VK_VERSION_PATCH (device_properties.apiVersion));
      g_print ("     Driver version: %u.%u.%u\n",
               VK_VERSION_MAJOR (device_properties.driverVersion),
               VK_VERSION_MINOR (device_properties.driverVersion),
               VK_VERSION_PATCH (device_properties.driverVersion));
      g_print ("     Multi-viewport: %u\n", device_features.multiViewport);

      for (j = 0; j < queue_family_count; j++)
        {
          g_print ("     Queue familly #%u:\n", j);
          g_print ("       Flags: ");
          if (queue_family_properties[j].queueFlags & VK_QUEUE_GRAPHICS_BIT)
            g_print ("G");
          if (queue_family_properties[j].queueFlags & VK_QUEUE_COMPUTE_BIT)
            g_print ("C");
          if (queue_family_properties[j].queueFlags & VK_QUEUE_TRANSFER_BIT)
            g_print ("T");
          if (queue_family_properties[j].queueFlags & VK_QUEUE_SPARSE_BINDING_BIT)
            g_print ("S");
          g_print ("\n");
          g_print ("       Count: %u\n", queue_family_properties[j].queueCount);
        }

      g_free (queue_family_properties);
    }

  vkDestroyInstance(instance, NULL);

  g_free (devices);
}

/* ex:set ts=2 sw=2 et: */
