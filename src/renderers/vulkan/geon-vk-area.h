/* This file is part of Geon: geon-vk-area.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GEON_TYPE_VK_AREA (geon_vk_area_get_type())

G_DECLARE_DERIVABLE_TYPE (GeonVKArea, geon_vk_area,
                          GEON, VK_AREA,
                          GtkWidget)

struct _GeonVKAreaClass
{
  /*< private >*/
  GtkWidgetClass parent_class;

  /*< public >*/
  gboolean            (*render)           (GeonVKArea         *area,
                                           GdkVulkanContext   *context);
  void                (*resize)           (GeonVKArea         *area,
                                           gint                width,
                                           gint                height);
  GdkVulkanContext   *(*create_context)   (GeonVKArea         *area);

  /*< private >*/
  gpointer _padding[6];
};

GtkWidget   *geon_vk_area_new   ();

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
