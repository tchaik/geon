/* This file is part of Geon: geon-vk-viewport.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-VKViewport"

#include "config.h"

#include <glib/gi18n.h>
#include <glib-object.h>
#include <vulkan/vulkan.h>

#include "geon-debug.h"
#include "geon-viewport.h"

#include "geon-vk-viewport.h"

typedef struct
{
  GeonTreeReference *root;
} GeonVKViewportPrivate;

struct _GeonVKViewport
{
  GeonVKArea parent_instance;
};

enum {
  PROP_0,
  PROP_ROOT,
  N_PROPS
};

static void geon_vk_viewport_iface_viewport_init (GeonViewportInterface *iface);

G_DEFINE_TYPE_EXTENDED (GeonVKViewport, geon_vk_viewport,
                        GEON_TYPE_VK_AREA, 0,
                        G_ADD_PRIVATE (GeonVKViewport)
                        G_IMPLEMENT_INTERFACE(GEON_TYPE_VIEWPORT,
                                              geon_vk_viewport_iface_viewport_init))

static GParamSpec *properties[N_PROPS];

static void
geon_vk_viewport_unset_root_cb (gpointer  data,
                                GObject  *object)
{
  GeonVKViewport *self = GEON_VK_VIEWPORT (data);
  GeonVKViewportPrivate *priv;

  g_assert (GEON_IS_VK_VIEWPORT (self));

  priv = geon_vk_viewport_get_instance_private (self);

  priv->root = NULL;
}

static void
geon_vk_viewport_set_root (GeonVKViewport    *self,
                           GeonTreeReference *root)
{
  GeonVKViewportPrivate *priv;

  g_assert (GEON_IS_VK_VIEWPORT (self));
  g_assert (!root || GEON_IS_TREE_REFERENCE (root));

  priv = geon_vk_viewport_get_instance_private (self);

  if (priv->root != root)
    {
      if (priv->root != NULL)
        {
          g_object_weak_unref (G_OBJECT (priv->root),
                               geon_vk_viewport_unset_root_cb,
                               self);
        }

      priv->root = root;

      if (priv->root != NULL)
        {
          g_object_weak_ref (G_OBJECT (priv->root),
                             geon_vk_viewport_unset_root_cb,
                             self);
        }

      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ROOT]);
    }
}

static void
geon_vk_viewport_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  GeonVKViewport *self = GEON_VK_VIEWPORT (object);

  g_return_if_fail (GEON_IS_VK_VIEWPORT (self));

  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_vk_viewport_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  GeonVKViewport *self = GEON_VK_VIEWPORT (object);

  g_return_if_fail (GEON_IS_VK_VIEWPORT (self));

  switch (property_id)
    {
    case PROP_ROOT:
      geon_vk_viewport_set_root (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_vk_viewport_finalize (GObject *object)
{
  GeonVKViewport *self = GEON_VK_VIEWPORT (object);
  GeonVKViewportPrivate *priv;

  g_return_if_fail (GEON_IS_VK_VIEWPORT (self));

  priv = geon_vk_viewport_get_instance_private (self);

  if (priv->root != NULL)
    {
      g_object_weak_unref (G_OBJECT (priv->root),
                           geon_vk_viewport_unset_root_cb,
                           self);
      priv->root = NULL;
    }

  G_OBJECT_CLASS (geon_vk_viewport_parent_class)->finalize (object);
}

static void
geon_vk_viewport_class_init (GeonVKViewportClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = geon_vk_viewport_get_property;
  object_class->set_property = geon_vk_viewport_set_property;
  object_class->finalize = geon_vk_viewport_finalize;

  properties[PROP_ROOT] =
    g_param_spec_object ("root",
                         "Root",
                         "The root reference rendered in that scene",
                         GEON_TYPE_TREE_REFERENCE,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
geon_vk_viewport_init (GeonVKViewport *self)
{
}

GeonVKViewport *
geon_vk_viewport_new ()
{
  return g_object_new (GEON_TYPE_VK_VIEWPORT, NULL);
}

gchar *
geon_vk_viewport_get_renderer_name (GeonViewport *viewport)
{
  GeonVKViewport *self = GEON_VK_VIEWPORT (viewport);

  g_return_val_if_fail (GEON_IS_VK_VIEWPORT (self), NULL);

  return g_strdup ("vulkan");
}

void
geon_vk_viewport_set_root_reference (GeonViewport      *viewport,
                                     GeonTreeReference *reference)
{
  GeonVKViewport *self = GEON_VK_VIEWPORT (viewport);

  g_return_if_fail (GEON_IS_VK_VIEWPORT (self));
  g_return_if_fail (!reference || GEON_IS_TREE_REFERENCE (reference));

  geon_vk_viewport_set_root (self, reference);
}

static void
geon_vk_viewport_iface_viewport_init (GeonViewportInterface *iface)
{
  iface->get_renderer_name = geon_vk_viewport_get_renderer_name;
  iface->set_root_reference = geon_vk_viewport_set_root_reference;
}

/* ex:set ts=2 sw=2 et: */
