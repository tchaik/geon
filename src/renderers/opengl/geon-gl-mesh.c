/* This file is part of Geon: geon-gl-                    mesh.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-GLMesh"

#include "config.h"

#include <glib/gi18n.h>
#include <epoxy/gl.h>

#include "geon-gl-mesh.h"

struct _GeonGLMesh
{
  GLuint vertex_buffer;
  GLuint index_buffer;
};

G_DEFINE_BOXED_TYPE (GeonGLMesh, geon_gl_mesh,
                     geon_gl_mesh_copy, geon_gl_mesh_free)

GeonGLMesh *
geon_gl_mesh_new (GeonMesh *mesh)
{
  GeonGLMesh *self = g_slice_new0 (GeonGLMesh);
  const GArray *vertex_array, *index_array;

  g_return_val_if_fail (mesh != NULL, NULL);

  if (self != NULL)
    {
      glGenBuffers (1, &self->vertex_buffer);

      vertex_array = geon_mesh_get_vertex_array (mesh);

      glBindBuffer(GL_ARRAY_BUFFER, self->vertex_buffer);
      glBufferData(GL_ARRAY_BUFFER,
                   vertex_array->len * sizeof (gfloat),
                   vertex_array->data, GL_STATIC_DRAW);

      glGenBuffers (1, &self->index_buffer);

      index_array = geon_mesh_get_index_array (mesh);

      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self->index_buffer);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                   index_array->len * sizeof (guint),
                   index_array->data, GL_STATIC_DRAW);
    }

  return self;
}

GeonGLMesh *
geon_gl_mesh_copy (const GeonGLMesh *self)
{
  g_assert_not_reached ();
}

void
geon_gl_mesh_free (GeonGLMesh *self)
{
  if (self != NULL)
    {
      glDeleteBuffers(1, &self->vertex_buffer);
      glDeleteBuffers(1, &self->index_buffer);
      g_slice_free (GeonGLMesh, self);
    }
}

/* ex:set ts=2 sw=2 et: */
