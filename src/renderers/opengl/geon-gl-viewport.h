/* This file is part of Geon: geon-gl-viewport.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gthree/gthreearea.h>

G_BEGIN_DECLS

#define GEON_TYPE_GL_VIEWPORT (geon_gl_viewport_get_type())

G_DECLARE_FINAL_TYPE (GeonGLViewport, geon_gl_viewport,
                      GEON, GL_VIEWPORT,
                      GthreeArea)

GeonGLViewport   *geon_gl_viewport_new   ();

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
