/* This file is part of Geon: geon-gl-utils.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-GLUtils"

#include "config.h"

#include <glib/gi18n.h>
#include <wayland-client.h>
#include <epoxy/egl.h>
#include <epoxy/gl.h>

#include "geon-debug.h"

#include "geon-gl-extensions.h"

#include "geon-gl-utils.h"

EGLDisplay
geon_gl_utils_get_egl_display ()
{
  struct wl_display *native_display;
  EGLDisplay egl_display = NULL;

  if (g_getenv ("XDG_RUNTIME_DIR") == NULL)
    return NULL;

  native_display = wl_display_connect (NULL);

  if (native_display == NULL)
    return NULL;

  if (epoxy_has_egl_extension (NULL, "EGL_KHR_platform_base"))
    {
      PFNEGLGETPLATFORMDISPLAYPROC eglGetPlatformDisplay =
        (void *) eglGetProcAddress ("eglGetPlatformDisplay");

      if (eglGetPlatformDisplay != NULL)
        egl_display = eglGetPlatformDisplay (EGL_PLATFORM_WAYLAND_EXT,
                                             native_display, NULL);
      if (egl_display != NULL)
        return egl_display;
    }

  if (epoxy_has_egl_extension (NULL, "EGL_EXT_platform_base"))
    {
      PFNEGLGETPLATFORMDISPLAYEXTPROC eglGetPlatformDisplayEXT =
        (void *) eglGetProcAddress ("eglGetPlatformDisplayEXT");

      if (eglGetPlatformDisplayEXT)
        egl_display = eglGetPlatformDisplayEXT (EGL_PLATFORM_WAYLAND_EXT,
                                                native_display, NULL);
      if (egl_display != NULL)
        return egl_display;
    }

  return eglGetDisplay ((EGLNativeDisplayType)native_display);
}

EGLDisplay
geon_gl_utils_init_egl ()
{
  EGLDisplay egl_display;
  EGLConfig config;
  EGLContext context;
  EGLint major, minor;
  EGLint count;

  static const EGLint config_attribs[] = {
    EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
    EGL_RENDERABLE_TYPE, EGL_OPENGL_BIT,
    EGL_NONE
  };

  static const EGLint context_attribs[] = {
    EGL_CONTEXT_CLIENT_VERSION, 2,
    EGL_NONE
  };

  egl_display = geon_gl_utils_get_egl_display ();

  if (egl_display == NULL)
    return NULL;

  if (!eglInitialize (egl_display, &major, &minor))
    return NULL;

  if (!eglBindAPI (EGL_OPENGL_API))
    return NULL;

  if (!eglChooseConfig (egl_display, config_attribs, &config, 1, &count))
    return NULL;

  context = eglCreateContext (egl_display, config, NULL, context_attribs);

  if (context == NULL)
    return NULL;

  eglMakeCurrent(egl_display, NULL, NULL, context);

  return egl_display;
}

void
geon_gl_utils_print_extension (gpointer data,
                               gpointer user_data)
{
  GeonGLExtension extension = GPOINTER_TO_UINT (data);
  const gchar *extension_name, *extension_string;
  GeonGLProvider extension_provider;
  const gchar *provider_name;

  extension_name = geon_extension_get_name (extension);
  extension_provider = geon_extension_get_provider (extension);
  extension_string = geon_extension_get_string (extension);

  provider_name = geon_extension_get_provider_name (extension);

  g_print ("      %s: %s (%s)\n",
           extension_name, extension_string, provider_name);
}

void
geon_gl_utils_print_info (void)
{
  EGLDisplay egl_display;
  gint egl_version, gl_version, glsl_version;
  gboolean status;

  g_print ("* OpenGL (gl/opengl):\n");

  egl_display = geon_gl_utils_init_egl ();

  egl_version = epoxy_egl_version (egl_display);
  gl_version = epoxy_gl_version ();
  glsl_version = epoxy_glsl_version ();

  status = geon_gl_extensions_detect (NULL);

  g_print ("   EGL API version: %d.%d\n", egl_version / 10, egl_version % 10);
  g_print ("   EGL version string: %s\n", eglQueryString(egl_display, EGL_VERSION));
  g_print ("   EGL vendor string: %s\n", eglQueryString(egl_display, EGL_VENDOR));

  g_print ("   GL API version: %d.%d\n", gl_version / 10, gl_version % 10);
  g_print ("   GL version string: %s\n", (gchar *) glGetString(GL_VERSION));
  g_print ("   GL renderer string: %s\n", (gchar *) glGetString(GL_RENDERER));
  g_print ("   GL vendor string: %s\n", (gchar *) glGetString(GL_VENDOR));

  g_print ("   GLSL API version: %d.%d\n", glsl_version / 100, gl_version % 100);

  g_print ("   GL extensions status:\n");

  geon_gl_extensions_foreach (geon_gl_utils_print_extension, NULL);

  eglTerminate (egl_display);
}

/* ex:set ts=2 sw=2 et: */
