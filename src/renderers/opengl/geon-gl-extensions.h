/* This file is part of Geon: geon-gl-extensions.h
 *
 * Copyright © 2019 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

typedef enum
{
  GEON_GL_EXTENSION_gpu_shader_fp64,
  GEON_GL_EXTENSION_vertex_attrib_64bit,
  GEON_GL_EXTENSION_vertex_buffer_object,
} GeonGLExtension;

typedef enum
{
  GEON_GL_PROVIDER_NONE,
  GEON_GL_PROVIDER_CORE,
  GEON_GL_PROVIDER_ARB,
  GEON_GL_PROVIDER_EXT,
  GEON_GL_PROVIDER_AMD,
  GEON_GL_PROVIDER_ATI,
  GEON_GL_PROVIDER_INTEL,
  GEON_GL_PROVIDER_MESA,
  GEON_GL_PROVIDER_NV,
  GEON_GL_PROVIDER_UNKNOWN,
} GeonGLProvider;

const gchar      *geon_extension_get_name            (GeonGLExtension extension);

GeonGLProvider    geon_extension_get_provider        (GeonGLExtension extension);

const gchar      *geon_extension_get_provider_name   (GeonGLExtension extension);

const gchar      *geon_extension_get_string          (GeonGLExtension extension);

gboolean   geon_gl_extensions_detect    (GError **error);

void       geon_gl_extensions_foreach   (GFunc    func,
                                         gpointer user_data);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
