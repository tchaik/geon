/* This file is part of Geon: geon-gl-extensions.c
 *
 * Copyright © 2019 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-GLExtensions"

#include "config.h"

#include <glib/gi18n.h>
#include <gio/gio.h>
#include <epoxy/gl.h>

#include "geon-debug.h"

#include "geon-gl-extensions.h"

static struct {
  const gchar    *name;

  const gint      core_version;
  const gboolean  is_required;
  const gchar    *strings[3];

  GeonGLProvider  provider;
  gchar          *string;
} extensions[] = {
  [GEON_GL_EXTENSION_gpu_shader_fp64] = {
    "GL.gpu_shader_fp64", 40, FALSE,
      { "GL_ARB_gpu_shader_fp64", NULL },
    GEON_GL_PROVIDER_NONE, NULL },

  [GEON_GL_EXTENSION_vertex_attrib_64bit] = {
    "GL.vertex_attrib_64bit", 46, FALSE,
      { "GL_ARB_vertex_attrib_64bit", NULL },
    GEON_GL_PROVIDER_NONE, NULL },

  [GEON_GL_EXTENSION_vertex_buffer_object] = {
    "GL.vertex_buffer_object", 15, TRUE,
      { "GL_ARB_vertex_buffer_object", NULL },
    GEON_GL_PROVIDER_NONE, NULL },
};

const gchar *
geon_extension_get_name (GeonGLExtension extension)
{
  return extensions[extension].name;
}

GeonGLProvider
geon_extension_get_provider (GeonGLExtension extension)
{
  return extensions[extension].provider;
}

const gchar *
geon_extension_get_provider_name (GeonGLExtension extension)
{
  switch (extensions[extension].provider)
    {
    case GEON_GL_PROVIDER_CORE:
      return "CORE";
    case GEON_GL_PROVIDER_ARB:
      return "ARB";
    case GEON_GL_PROVIDER_EXT:
      return "EXT";
    case GEON_GL_PROVIDER_AMD:
      return "ARB";
    case GEON_GL_PROVIDER_ATI:
      return "ATI";
    case GEON_GL_PROVIDER_INTEL:
      return "INTEL";
    case GEON_GL_PROVIDER_MESA:
      return "MESA";
    case GEON_GL_PROVIDER_NV:
      return "NV";
      case GEON_GL_PROVIDER_UNKNOWN:
      return "UNKNOWN";
    case GEON_GL_PROVIDER_NONE:
    default:
      return NULL;
    }
}

const gchar *
geon_extension_get_string (GeonGLExtension extension)
{
  return extensions[extension].string;
}

GeonGLProvider
geon_gl_extension_probe_provider (const gchar *extension_string)
{
  if (strstr (extension_string, "ARB_") != NULL)
    return GEON_GL_PROVIDER_ARB;
  else if (strstr (extension_string, "EXT_") != NULL)
    return GEON_GL_PROVIDER_EXT;
  else if (strstr (extension_string, "AMD") != NULL)
    return GEON_GL_PROVIDER_AMD;
  else if (strstr (extension_string, "ATI_") != NULL)
    return GEON_GL_PROVIDER_ATI;
  else if (strstr (extension_string, "INTEL_") != NULL)
    return GEON_GL_PROVIDER_INTEL;
  else if (strstr (extension_string, "MESA_") != NULL)
    return GEON_GL_PROVIDER_MESA;
  else if (strstr (extension_string, "NV_") != NULL)
    return GEON_GL_PROVIDER_NV;
  else
    return GEON_GL_PROVIDER_UNKNOWN;
}

gpointer
geon_gl_extensions_detect_worker (gpointer data)
{
  GError **error = data;
  gint gl_version, glsl_version;
  guint i;

  gl_version = epoxy_gl_version ();
  glsl_version = epoxy_glsl_version ();

  for (i = 0; i < G_N_ELEMENTS (extensions); i++)
    {
      guint j = 0;

      if (extensions[i].core_version <= gl_version)
        {
          extensions[i].provider = GEON_GL_PROVIDER_CORE;
          continue;
        }

      while (extensions[i].strings[j] != NULL)
        {
          const gchar *string = extensions[i].strings[j];
          GeonGLProvider provider;

          if (epoxy_has_gl_extension (string))
            {
              provider = geon_gl_extension_probe_provider (string);

              extensions[i].string = (gchar *)string;
              extensions[i].provider = provider;
              break;
            }

          j++;
        }

      if (extensions[i].is_required
       && extensions[i].provider == GEON_GL_PROVIDER_NONE)
        {
          g_set_error (error,
                       G_IO_ERROR,
                       G_IO_ERROR_NOT_SUPPORTED,
                       "Required extension not supported: %s",
                       extensions[i].name);
          return GINT_TO_POINTER (FALSE);
        }
    }

  return GINT_TO_POINTER (TRUE);
}

gboolean
geon_gl_extensions_detect (GError **error)
{
  static GOnce init = G_ONCE_INIT;

  g_once (&init, geon_gl_extensions_detect_worker, error);

  return GPOINTER_TO_INT (init.retval);
}

void
geon_gl_extensions_foreach (GFunc    func,
                            gpointer user_data)
{
  guint i;

  g_return_if_fail (func != NULL);

  for (i = 0; i < G_N_ELEMENTS (extensions); i++)
    (*func) (GUINT_TO_POINTER (i), user_data);
}

/* ex:set ts=2 sw=2 et: */
