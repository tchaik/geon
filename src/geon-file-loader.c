/* This file is part of Geon: geon-file-loader.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-FileLoader"

#include "config.h"

#include <glib/gi18n.h>

#include "geon-debug.h"

#include "geon-file-loader.h"

G_DEFINE_INTERFACE (GeonFileLoader, geon_file_loader,
                    G_TYPE_OBJECT)

static void
geon_file_loader_default_init (GeonFileLoaderInterface *iface)
{
  iface->initialize = NULL;
  iface->load_tree = NULL;
  iface->load_geonmetry = NULL;
}

gboolean
geon_file_loader_initialize (GeonFileLoader  *self,
                             GeonFileFormat  *file_format,
                             GFile           *file,
                             GCancellable    *cancellable,
                             GError         **error)
{
  g_return_val_if_fail (GEON_IS_FILE_LOADER (self), FALSE);
  g_return_val_if_fail (file_format != NULL, FALSE);
  g_return_val_if_fail (G_IS_FILE (file), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  if (GEON_FILE_LOADER_GET_IFACE (self)->initialize)
    return GEON_FILE_LOADER_GET_IFACE (self)->initialize (self,
                                                          file_format, file,
                                                          cancellable,
                                                          error);

  return FALSE;
}

GeonTreeReference *
geon_file_loader_load_tree (GeonFileLoader  *self,
                            GCancellable    *cancellable,
                            GError         **error)
{
  g_return_val_if_fail (GEON_IS_FILE_LOADER (self), NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  if (GEON_FILE_LOADER_GET_IFACE (self)->load_tree)
    return GEON_FILE_LOADER_GET_IFACE (self)->load_tree (self,
                                                         cancellable,
                                                         error);

  return NULL;
}

gboolean
geon_file_loader_load_geometry (GeonFileLoader  *self,
                                GCancellable    *cancellable,
                                GError         **error)
{
  g_return_val_if_fail (GEON_IS_FILE_LOADER (self), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  if (GEON_FILE_LOADER_GET_IFACE (self)->load_geonmetry)
    return GEON_FILE_LOADER_GET_IFACE (self)->load_geonmetry (self,
                                                              cancellable,
                                                              error);

  return FALSE;
}

/* ex:set ts=2 sw=2 et: */
