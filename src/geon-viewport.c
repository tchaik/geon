/* This file is part of Geon: geon-viewport.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-Viewport"

#include "config.h"

#include <glib/gi18n.h>
#include <glib-object.h>

#include "geon-debug.h"

#include "geon-viewport.h"

enum {
  PROP_0,
  PROP_ROOT,
  N_PROPS
};

G_DEFINE_INTERFACE (GeonViewport, geon_viewport,
                    GTK_TYPE_WIDGET)

static GParamSpec *properties[N_PROPS];

static void
geon_viewport_default_init (GeonViewportInterface *iface)
{
  iface->get_renderer_name = geon_viewport_get_renderer_name;

  properties[PROP_ROOT] =
    g_param_spec_object ("root",
                         "Root",
                         "The 3D root reference rendered in that scene",
                         GEON_TYPE_TREE_REFERENCE,
                         (G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));

  g_object_interface_install_property (iface, properties[PROP_ROOT]);
}

gchar *
geon_viewport_get_renderer_name (GeonViewport *self)
{
  g_return_val_if_fail (GEON_IS_VIEWPORT (self), NULL);

  if (GEON_VIEWPORT_GET_IFACE (self)->get_renderer_name)
      return GEON_VIEWPORT_GET_IFACE (self)->get_renderer_name (self);

  return NULL;
}

void
geon_viewport_set_root_reference (GeonViewport      *self,
                                  GeonTreeReference *reference)
{
  g_return_if_fail (GEON_IS_VIEWPORT (self));
  g_return_if_fail (!reference || GEON_IS_TREE_REFERENCE (reference));

  if (GEON_VIEWPORT_GET_IFACE (self)->set_root_reference)
      GEON_VIEWPORT_GET_IFACE (self)->set_root_reference (self, reference);
}

/* ex:set ts=2 sw=2 et: */
