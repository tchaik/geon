/* This file is part of Geon: geon-stl-file-loader.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-STLFileLoader"

#include <glib/gi18n.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libpeas/peas.h>

#include "geon-debug.h"
#include "geon-file-formats.h"
#include "geon-file-loader.h"
#include "geon-mesh.h"
#include "geon-tree-reference.h"

typedef struct
{
  GFile             *file;

  gchar             *file_name;
  gchar             *format_name;
  gchar             *solid_name;

  GDataInputStream  *data_stream;

  guint32            nb_triangles;

  gboolean           initialized;

  GeonTreeReference *reference;

  gboolean           tree_loaded;

  GeonMesh          *mesh;

  gboolean           geometry_loaded;
} GeonSTLFileLoaderPrivate;

struct _GeonSTLFileLoader
{
  GObject parent_instance;
};

#define GEON_TYPE_STL_FILE_LOADER (geon_stl_file_loader_get_type ())

G_DECLARE_FINAL_TYPE (GeonSTLFileLoader, geon_stl_file_loader,
                      GEON, STL_FILE_LOADER,
                      GObject)

static void geon_stl_file_loader_iface_init (GeonFileLoaderInterface *iface);

G_DEFINE_TYPE_EXTENDED (GeonSTLFileLoader, geon_stl_file_loader,
                        G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (GEON_TYPE_FILE_LOADER,
                                               geon_stl_file_loader_iface_init)
                        G_ADD_PRIVATE (GeonSTLFileLoader))

#define ASCII_SOLID_BLOCK_START "solid"
#define ASCII_SOLID_BLOCK_END   "endsolid"
#define ASCII_FACET_BLOCK_START "facet"
#define ASCII_FACET_BLOCK_END   "endfacet"
#define ASCII_NORMAL_MARKER     "normal"
#define ASCII_LOOP_BLOCK_START  "outer loop"
#define ASCII_LOOP_BLOCK_END    "endloop"
#define ASCII_VERTEX_MARKER     "vertex"

#define BINARY_HEADER_SIZE      80
#define BINARY_BLOCK_SIZE       50
#define BINARY_NORMAL_OFFSET    0
#define BINARY_NORMAL_SIZE      12
#define BINARY_VERTICES_OFFSET  12
#define BINARY_VERTICES_SIZE    36
#define BINARY_VERTICE_SIZE     12
#define BINARY_ATTRIBUTE_OFFSET 48
#define BINARY_ATTRIBUTE_SIZE   2

static void
geon_stl_file_loader_dispose (GObject *object)
{
  GeonSTLFileLoader *self = GEON_STL_FILE_LOADER (object);
  GeonSTLFileLoaderPrivate *priv;

  g_return_if_fail (GEON_IS_STL_FILE_LOADER (self));

  priv = geon_stl_file_loader_get_instance_private (self);

  g_clear_object (&priv->reference);

  g_clear_object (&priv->data_stream);

  G_OBJECT_CLASS (geon_stl_file_loader_parent_class)->dispose (object);
}

static void
geon_stl_file_loader_finalize (GObject *object)
{
  GeonSTLFileLoader *self = GEON_STL_FILE_LOADER (object);
  GeonSTLFileLoaderPrivate *priv;

  g_return_if_fail (GEON_IS_STL_FILE_LOADER (self));

  priv = geon_stl_file_loader_get_instance_private (self);

  g_clear_pointer (&priv->mesh, geon_mesh_unref);

  g_clear_pointer (&priv->solid_name, g_free);
  g_clear_pointer (&priv->format_name, g_free);
  g_clear_pointer (&priv->file_name, g_free);

  g_clear_object (&priv->file);

  G_OBJECT_CLASS (geon_stl_file_loader_parent_class)->finalize (object);
}

static void
geon_stl_file_loader_class_init (GeonSTLFileLoaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = geon_stl_file_loader_dispose;
  object_class->finalize = geon_stl_file_loader_finalize;
}

static void
geon_stl_file_loader_init (GeonSTLFileLoader *self)
{
  GeonSTLFileLoaderPrivate *priv;

  priv = geon_stl_file_loader_get_instance_private (self);

  priv->geometry_loaded = FALSE;
  priv->tree_loaded = FALSE;
  priv->initialized = FALSE;
}

static gboolean
geon_stl_file_loader_load_ascii_header (GeonSTLFileLoader  *self,
                                        GCancellable       *cancellable,
                                        GError            **error)
{
  GeonSTLFileLoaderPrivate *priv;
  g_autofree gchar *line = NULL;
  gsize data_read = 0;

  g_assert (GEON_IS_STL_FILE_LOADER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_stl_file_loader_get_instance_private (self);

  g_assert (priv->data_stream != NULL);

  line = g_data_input_stream_read_line (priv->data_stream,
                                        &data_read,
                                        cancellable,
                                        error);
  if (line == NULL)
    return FALSE;
  else if (!g_str_has_prefix (line, ASCII_SOLID_BLOCK_START))
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_INVALID_DATA,
                   "Given STL file does not describe a solid");
      return FALSE;
    }

  priv->nb_triangles = 0;

  return TRUE;
}

static gboolean
geon_stl_file_loader_load_binary_header (GeonSTLFileLoader  *self,
                                         GCancellable       *cancellable,
                                         GError            **error)
{
  GeonSTLFileLoaderPrivate *priv;
  guint32 nb_triangles;
  gboolean status;

  g_assert (GEON_IS_STL_FILE_LOADER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_stl_file_loader_get_instance_private (self);

  g_assert (priv->data_stream != NULL);

  status = g_seekable_seek (G_SEEKABLE (priv->data_stream),
                            BINARY_HEADER_SIZE, G_SEEK_SET,
                            cancellable,
                            error);
  if (*error != NULL && status == FALSE)
    return FALSE;

  nb_triangles = g_data_input_stream_read_uint32 (priv->data_stream,
                                                  cancellable,
                                                  error);
  if (*error != NULL)
    return FALSE;

  priv->nb_triangles = nb_triangles;

  return TRUE;
}

static gboolean
geon_stl_file_loader_initialize (GeonFileLoader   *loader,
                                 GeonFileFormat   *file_format,
                                 GFile            *file,
                                 GCancellable    *cancellable,
                                 GError         **error)
{
  GeonSTLFileLoader *self = GEON_STL_FILE_LOADER (loader);
  GeonSTLFileLoaderPrivate *priv;
  g_autoptr(GInputStream) input_stream = NULL;
  g_autoptr(GDataInputStream) data_stream = NULL;
  g_autoptr(GFileInfo) file_info = NULL;
  const gchar *file_name, *base_name, *file_extension;
  const gchar *format_name;
  gboolean status;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_STL_FILE_LOADER (self), FALSE);
  g_return_val_if_fail (file_format != NULL, FALSE);
  g_return_val_if_fail (G_IS_FILE (file), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  priv = geon_stl_file_loader_get_instance_private (self);

  format_name = geon_file_format_get_name (file_format);

  file_info = g_file_query_info (file,
                                 G_FILE_ATTRIBUTE_STANDARD_NAME","
                                 G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
                                 G_FILE_QUERY_INFO_NONE,
                                 cancellable,
                                 error);
  if (file_info == NULL)
    GEON_RETURN_VAL (FALSE);

  file_name =
    g_file_info_get_attribute_string (file_info,
                                      G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME);
  if (file_name == NULL)
    {
      base_name =
        g_file_info_get_attribute_string (file_info,
                                          G_FILE_ATTRIBUTE_STANDARD_NAME);
      file_name = g_utf8_make_valid (base_name, -1);
    }

  file_extension = g_strrstr (file_name, ".");

  input_stream = G_INPUT_STREAM (g_file_read (file, cancellable, error));
  if (input_stream == NULL)
    GEON_RETURN_VAL (FALSE);

  data_stream = g_data_input_stream_new (input_stream);

  g_assert (G_IS_DATA_INPUT_STREAM (data_stream));

  priv->file = g_file_dup (file);
  priv->file_name = g_strdup (file_name);
  priv->format_name = g_strdup (format_name);
  priv->solid_name = g_strndup (priv->file_name,
                                strlen (file_name) - strlen (file_extension));

  priv->data_stream = g_steal_pointer (&data_stream);

  g_data_input_stream_set_byte_order (priv->data_stream,
                                      G_DATA_STREAM_BYTE_ORDER_LITTLE_ENDIAN);

  if (g_strcmp0 (format_name, "STL-ASCII") == 0)
    {
      status = geon_stl_file_loader_load_ascii_header (self,
                                                       cancellable,
                                                       error);
    }
  else if (g_strcmp0 (format_name, "STL-binary") == 0)
    {
      status = geon_stl_file_loader_load_binary_header (self,
                                                        cancellable,
                                                        error);
    }
  else
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "File loader must be initialized first");
      status = FALSE;
    }

    if (status == FALSE)
      GEON_RETURN_VAL (FALSE);

  priv->initialized = TRUE;

  GEON_RETURN_VAL (TRUE);
}

static GeonTreeReference *
geon_stl_file_loader_load_tree (GeonFileLoader  *loader,
                                GCancellable    *cancellable,
                                GError         **error)
{
  GeonSTLFileLoader *self = GEON_STL_FILE_LOADER (loader);
  GeonSTLFileLoaderPrivate *priv;
  g_autoptr(GeonTreeReference) reference = NULL;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_STL_FILE_LOADER (self), NULL);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), NULL);

  priv = geon_stl_file_loader_get_instance_private (self);

  if (!priv->initialized || priv->data_stream == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_INITIALIZED,
                   "File loader must be initialized first");
      GEON_RETURN_VAL (NULL);
    }
  else if (priv->tree_loaded == TRUE)
    return priv->reference;

  reference = geon_tree_reference_new (priv->solid_name);

  priv->reference = g_steal_pointer (&reference);

  g_info ("From '%s' extracted tree of 1 reference: '%s'",
          priv->file_name, priv->solid_name);

  priv->tree_loaded = TRUE;

  GEON_RETURN_VAL (priv->reference);
}

static gboolean
geon_stl_file_loader_load_ascii_geometry (GeonSTLFileLoader  *self,
                                          GCancellable       *cancellable,
                                          GError            **error)
{
  GeonSTLFileLoaderPrivate *priv;
  g_autoptr(GeonMesh) mesh = NULL;
  gboolean read_facet = FALSE;
  gboolean read_loop = FALSE;
  gfloat vertex_array[9];
  guint32 nb_triangles = 0;
  guint i = 0;

  g_assert (GEON_IS_STL_FILE_LOADER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_stl_file_loader_get_instance_private (self);

  g_assert (priv->data_stream != NULL);
  g_assert (priv->nb_triangles == 0);

  mesh = geon_mesh_new (GEON_MESH_DIMENSION_3D,
                        GEON_MESH_PRECISION_SINGLE);

  g_assert (mesh != NULL);

  while (!g_cancellable_is_cancelled (cancellable))
    {
      g_autofree gchar *line = NULL;
      gchar *content, **vertex;
      gsize data_read = 0;

      line = g_data_input_stream_read_line (priv->data_stream,
                                            &data_read,
                                            cancellable,
                                            error);
      if (*error != NULL)
        return FALSE;
      else if (line == NULL)
        break;

      content = g_strstrip (line);

      if (g_str_has_prefix (content, ASCII_SOLID_BLOCK_END))
        break;

      if (read_facet == FALSE)
        {
          if (!g_str_has_prefix (content, ASCII_FACET_BLOCK_START))
            return FALSE;

          read_facet = TRUE;
          continue;
        }
      else if (g_str_has_prefix (content, ASCII_FACET_BLOCK_END))
        {
          geon_mesh_add_elements (mesh,
                                  GEON_MESH_ELEMENT_TYPE_TRIANGLE, 1,
                                  vertex_array, sizeof (vertex_array));

          read_facet = FALSE; nb_triangles++;
          continue;
        }

      if (read_loop == FALSE)
        {
          if (!g_str_has_prefix (content, ASCII_LOOP_BLOCK_START))
            return FALSE;

          read_loop = TRUE; i = 0;
          continue;
        }
      else if (g_str_has_prefix (content, ASCII_LOOP_BLOCK_END))
        {
          read_loop = FALSE; g_assert (i == 9);
          continue;
        }

      if (!g_str_has_prefix (content, ASCII_VERTEX_MARKER))
        return FALSE;

      content += sizeof (ASCII_VERTEX_MARKER) + 1;
      vertex = g_strsplit (content, " ", 3);

      vertex_array[i++] = strtof (vertex[0], NULL);
      vertex_array[i++] = strtof (vertex[1], NULL);
      vertex_array[i++] = strtof (vertex[2], NULL);

      g_strfreev (vertex);
    }

  priv->mesh = g_steal_pointer (&mesh);
  priv->nb_triangles = nb_triangles;

  return TRUE;
}

static gboolean
geon_stl_file_loader_load_binary_geometry (GeonSTLFileLoader  *self,
                                           GCancellable       *cancellable,
                                           GError            **error)
{
  GeonSTLFileLoaderPrivate *priv;
  g_autoptr(GeonMesh) mesh = NULL;
  g_autofree guint8 *buffer = NULL;
  const gsize buffer_size =
    BINARY_NORMAL_SIZE + BINARY_VERTICES_SIZE;
  guint i;

  g_assert (GEON_IS_STL_FILE_LOADER (self));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_stl_file_loader_get_instance_private (self);

  g_assert (priv->data_stream != NULL);
  g_assert (priv->nb_triangles > 0);

  mesh = geon_mesh_new (GEON_MESH_DIMENSION_3D,
                        GEON_MESH_PRECISION_SINGLE);

  g_assert (mesh != NULL);

  geon_mesh_allocate_elements (mesh,
                               GEON_MESH_ELEMENT_TYPE_TRIANGLE,
                               priv->nb_triangles);

  buffer = g_malloc (buffer_size);

  g_assert (buffer != NULL);

  for (i = 0; i < priv->nb_triangles; i++)
    {
      gssize read =
        g_input_stream_read (G_INPUT_STREAM (priv->data_stream),
                             buffer, buffer_size,
                             cancellable,
                             error);
      if (*error != NULL)
        return FALSE;
      else if (read == 0)
        break;

      geon_mesh_add_elements (mesh,
                              GEON_MESH_ELEMENT_TYPE_TRIANGLE, 1,
                              buffer + BINARY_NORMAL_SIZE,
                              3 * BINARY_VERTICE_SIZE);
    }

  priv->mesh = g_steal_pointer (&mesh);

  return TRUE;
}

static gboolean
geon_stl_file_loader_load_geometry (GeonFileLoader  *loader,
                                    GCancellable    *cancellable,
                                    GError         **error)
{
  GeonSTLFileLoader *self = GEON_STL_FILE_LOADER (loader);
  GeonSTLFileLoaderPrivate *priv;
  gboolean status = FALSE;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_STL_FILE_LOADER (self), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  priv = geon_stl_file_loader_get_instance_private (self);

  g_assert (priv->reference != NULL);

  if (!priv->initialized || priv->data_stream == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_INITIALIZED,
                   "Loader must be initialized first");
      GEON_RETURN_VAL (FALSE);
    }
  else if (!priv->tree_loaded || priv->reference == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_INITIALIZED,
                   "Tree must be loaded first");
      GEON_RETURN_VAL (FALSE);
    }
  else if (priv->geometry_loaded == TRUE)
    GEON_RETURN_VAL (TRUE);

  if (g_strcmp0 (priv->format_name, "STL-ASCII") == 0)
    {
      status = geon_stl_file_loader_load_ascii_geometry (self,
                                                         cancellable,
                                                         error);
    }
  else if (g_strcmp0 (priv->format_name, "STL-binary") == 0)
    {
      status = geon_stl_file_loader_load_binary_geometry (self,
                                                          cancellable,
                                                          error);
    }

    if (status == FALSE)
      GEON_RETURN_VAL (FALSE);

  g_assert (priv->mesh != NULL);

  geon_tree_reference_set_representation (priv->reference,
                                          priv->mesh);

  g_input_stream_close (G_INPUT_STREAM (priv->data_stream),
                        cancellable, NULL);

  g_info ("From '%s' loaded mesh of %u triangles",
          priv->file_name, priv->nb_triangles);

  priv->geometry_loaded = TRUE;

  GEON_RETURN_VAL (TRUE);
}

static void
geon_stl_file_loader_iface_init (GeonFileLoaderInterface *iface)
{
  iface->initialize = geon_stl_file_loader_initialize;
  iface->load_tree = geon_stl_file_loader_load_tree;
  iface->load_geonmetry = geon_stl_file_loader_load_geometry;
}

extern void
geon_stl_file_loader_register_types (PeasObjectModule *module)
{
  peas_object_module_register_extension_type (module,
                                              GEON_TYPE_FILE_LOADER,
                                              GEON_TYPE_STL_FILE_LOADER);
}

/* ex:set ts=2 sw=2 et: */
