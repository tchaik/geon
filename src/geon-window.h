/* This file is part of Geon: geon-window.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

#include <gtk/gtk.h>

#include "geon-world.h"

G_BEGIN_DECLS

#define GEON_TYPE_WINDOW (geon_window_get_type())

G_DECLARE_FINAL_TYPE (GeonWindow, geon_window,
                      GEON, WINDOW,
                      GtkApplicationWindow)

GeonWindow   *geon_window_new               (GtkApplication *application);

gboolean      geon_window_is_empty          (GeonWindow *self);

void          geon_window_open_uri          (GeonWindow  *self,
                                             const gchar *file_uri);

void          geon_window_open_file         (GeonWindow *self,
                                             GFile      *file);

GeonWorld    *geon_window_get_world         (GeonWindow *self);

GFile        *geon_window_get_opened_file   (GeonWindow *self);

void          geon_window_close             (GeonWindow *self,
                                             gboolean    keep_empty);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
