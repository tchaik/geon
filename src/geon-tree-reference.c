/* This file is part of Geon: geon-tree-reference.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-TreeReference"

#include "config.h"

#include <glib/gi18n.h>

#include "geon-debug.h"

#include "geon-tree-reference.h"

typedef struct
{
  gchar    *name;

  GeonMesh *mesh;
} GeonTreeReferencePrivate;

struct _GeonTreeReference
{
  GObject parent_instance;
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_REPRESENTATION,
  N_PROPS
};

G_DEFINE_TYPE_WITH_PRIVATE (GeonTreeReference, geon_tree_reference,
                            G_TYPE_OBJECT)

static GParamSpec *properties[N_PROPS];

static void
geon_tree_reference_get_property (GObject    *object,
                                  guint       property_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  GeonTreeReference *self = GEON_TREE_REFERENCE (object);

  g_return_if_fail (GEON_IS_TREE_REFERENCE (self));

  switch (property_id)
    {
    case PROP_NAME:
      g_value_set_string (value, geon_tree_reference_get_name (self));
      break;

    case PROP_REPRESENTATION:
      g_value_set_boxed (value, geon_tree_reference_get_representation (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_tree_reference_set_property (GObject      *object,
                                  guint         property_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  GeonTreeReference *self = GEON_TREE_REFERENCE (object);

  g_return_if_fail (GEON_IS_TREE_REFERENCE (self));

  switch (property_id)
    {
    case PROP_NAME:
      geon_tree_reference_set_name (self, g_value_get_string (value));
      break;

    case PROP_REPRESENTATION:
      geon_tree_reference_set_representation (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_tree_reference_finalize (GObject *object)
{
  GeonTreeReference *self = GEON_TREE_REFERENCE (object);
  GeonTreeReferencePrivate *priv;

  g_return_if_fail (GEON_IS_TREE_REFERENCE (self));

  priv = geon_tree_reference_get_instance_private (self);

  g_free (priv->name);

  G_OBJECT_CLASS (geon_tree_reference_parent_class)->finalize (object);
}

static void
geon_tree_reference_class_init (GeonTreeReferenceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = geon_tree_reference_get_property;
  object_class->set_property = geon_tree_reference_set_property;
  object_class->finalize = geon_tree_reference_finalize;

  properties[PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The reference name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties[PROP_REPRESENTATION] =
    g_param_spec_string ("representation",
                         "Representation",
                         "The 3D repredentation",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
geon_tree_reference_init (GeonTreeReference *self)
{
}

GeonTreeReference *
geon_tree_reference_new (const gchar *name)
{
  g_return_val_if_fail (name != NULL, NULL);

  return g_object_new (GEON_TYPE_TREE_REFERENCE,
                       "name", name,
                       NULL);
}

const gchar *
geon_tree_reference_get_name (GeonTreeReference *self)
{
  GeonTreeReferencePrivate *priv;

  g_return_val_if_fail (GEON_IS_TREE_REFERENCE (self), NULL);

  priv = geon_tree_reference_get_instance_private (self);

  return priv->name;
}

void
geon_tree_reference_set_name (GeonTreeReference *self,
                              const gchar       *name)
{
  GeonTreeReferencePrivate *priv;

  g_return_if_fail (GEON_IS_TREE_REFERENCE (self));
  g_return_if_fail (name != NULL);

  priv = geon_tree_reference_get_instance_private (self);

  if (!g_utf8_validate (name, -1, NULL))
    priv->name = g_utf8_make_valid (name, -1);
  else
    priv->name = g_strdup (name);

  g_object_notify_by_pspec (G_OBJECT (self),
                            properties[PROP_NAME]);
}

GeonMesh *
geon_tree_reference_get_representation (GeonTreeReference *self)
{
  GeonTreeReferencePrivate *priv;

  g_return_val_if_fail (GEON_IS_TREE_REFERENCE (self), NULL);

  priv = geon_tree_reference_get_instance_private (self);

  return priv->mesh;
}

void
geon_tree_reference_set_representation (GeonTreeReference *self,
                                        GeonMesh          *mesh)
{
  GeonTreeReferencePrivate *priv;

  g_return_if_fail (GEON_IS_TREE_REFERENCE (self));
  g_return_if_fail (mesh != NULL);

  priv = geon_tree_reference_get_instance_private (self);

  if (priv->mesh != mesh)
    {
      priv->mesh = geon_mesh_ref (mesh);

      g_object_notify_by_pspec (G_OBJECT (self),
                                properties[PROP_REPRESENTATION]);
    }
}

/* ex:set ts=2 sw=2 et: */
