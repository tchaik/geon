/* This file is part of Geon: geon-world.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-World"

#include "config.h"

#include <glib/gi18n.h>
#include <gio/gio.h>

#include "geon-application-private.h"
#include "geon-debug.h"
#include "geon-file-analyzer.h"
#include "geon-file-formats.h"
#include "geon-file-loader.h"
#include "geon-tree.h"
#include "geon-tree-reference.h"
#include "geon-tree-private.h"

#include "geon-world.h"

typedef struct
{
  GFile          *file;

  gchar          *name;
  gchar          *content_type;
  guint64         size;

  gboolean        writable;

  GeonFileFormat *format;
} GeonWorldFile;

typedef struct
{
  GeonFileLoader    *loader;

  GeonTreeReference *reference;

  GtkTreeModel      *tree;
} GeonWorldTree;

typedef struct
{
  GFile             *file;

  gchar             *file_name;
  gboolean           file_is_writable;
  guint64            file_size;

  GeonFileFormat    *file_format;

  GeonFileLoader    *file_loader;

  GeonTreeReference *root_reference;
  GtkTreeModel      *root_tree;
} GeonWorldPrivate;

struct _GeonWorld
{
  GObject parent_instance;
};

enum {
  PROP_0,
  PROP_FILE,
  PROP_TREE,
  N_PROPS
};

static void geon_world_initable_iface_init (GInitableIface *iface);
static void geon_world_async_initable_iface_init (GAsyncInitableIface *iface);

G_DEFINE_TYPE_EXTENDED (GeonWorld, geon_world,
                        G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                               geon_world_initable_iface_init)
                        G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE,
                                               geon_world_async_initable_iface_init)
                        G_ADD_PRIVATE (GeonWorld))

#define HEADER_MAX_REQUEST_SIZE 4096

static GParamSpec *properties[N_PROPS];

static GeonWorldFile *
geon_world_file_new (GFile *file)
{
  GeonWorldFile *self = g_slice_new0 (GeonWorldFile);

  if (self != NULL)
    g_set_object (&self->file, file);

  return self;
}

static void
geon_world_file_free (gpointer data)
{
  GeonWorldFile *self = (GeonWorldFile *)data;

  if (self != NULL)
    {
      self->format = NULL;
      g_clear_pointer (&self->content_type, g_free);
      g_clear_pointer (&self->name, g_free);
      g_clear_object (&self->file);
      g_slice_free (GeonWorldFile, self);
    }
}

static GeonWorldTree *
geon_world_tree_new ()
{
  return g_slice_new0 (GeonWorldTree);
}

static void
geon_world_tree_free (gpointer data)
{
  GeonWorldTree *self = (GeonWorldTree *)data;

  if (self != NULL)
    {
      g_clear_object (&self->tree);
      g_clear_object (&self->reference);
      g_clear_object (&self->loader);
      g_slice_free (GeonWorldTree, self);
    }
}

static GeonWorldFile *
geon_world_do_query_file_info (GeonWorld     *self,
                               GFile         *file,
                               GCancellable  *cancellable,
                               GError       **error)
{
  GeonWorldPrivate *priv;
  GeonWorldFile *world_file;
  g_autoptr(GFileInfo) file_info = NULL;
  GFileType file_type;
  gboolean can_read, can_write;
  g_autofree const gchar *base_name = NULL;
  const gchar *name, *content_type;
  guint64 size;

  g_assert (GEON_IS_WORLD (self));
  g_assert (G_IS_FILE (file));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_world_get_instance_private (self);

  file_info = g_file_query_info (file,
                                 G_FILE_ATTRIBUTE_STANDARD_TYPE","
                                 G_FILE_ATTRIBUTE_STANDARD_NAME","
                                 G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME","
                                 G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE","
                                 G_FILE_ATTRIBUTE_STANDARD_SIZE","
                                 G_FILE_ATTRIBUTE_ACCESS_CAN_READ","
                                 G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE,
                                 G_FILE_QUERY_INFO_NONE,
                                 cancellable,
                                 error);

  if (file_info == NULL && error == NULL)
    return NULL;
  else if (file_info == NULL)
    {
      if (*error == NULL)
        g_set_error (error,
                     G_IO_ERROR,
                     G_IO_ERROR_FAILED,
                     "Given location cannot be queried");
      return NULL;
    }

  file_type =
    g_file_info_get_attribute_uint32 (file_info,
                                      G_FILE_ATTRIBUTE_STANDARD_TYPE);
  if (file_type != G_FILE_TYPE_REGULAR)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_REGULAR_FILE,
                   "Given location does not point a file");
      return NULL;
    }

  can_read =
    g_file_info_get_attribute_boolean (file_info,
                                       G_FILE_ATTRIBUTE_ACCESS_CAN_READ);
  can_write =
    g_file_info_get_attribute_boolean (file_info,
                                       G_FILE_ATTRIBUTE_ACCESS_CAN_WRITE);

  if (can_read == FALSE)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_PERMISSION_DENIED,
                   "Given location is not readable");
      return NULL;
    }

  name =
    g_file_info_get_attribute_string (file_info,
                                      G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME);
  content_type =
    g_file_info_get_attribute_string (file_info,
                                      G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE);
  size =
    g_file_info_get_attribute_uint64 (file_info,
                                      G_FILE_ATTRIBUTE_STANDARD_SIZE);

  if (name == NULL)
    {
      base_name =
        g_file_info_get_attribute_string (file_info,
                                          G_FILE_ATTRIBUTE_STANDARD_NAME);
      name = g_utf8_make_valid (base_name, -1);
    }

  world_file = geon_world_file_new (file);

  g_assert (world_file != NULL);

  world_file->name = g_strdup (name);
  world_file->content_type = g_strdup (content_type);
  world_file->size = size;
  world_file->writable = can_write;

  world_file->format = NULL;

  return world_file;
}

static gboolean
geon_world_do_analyze_file (GeonWorld      *self,
                            GeonWorldFile  *world_file,
                            GCancellable   *cancellable,
                            GError        **error)
{
  GeonWorldPrivate *priv;
  g_autofree const gchar *file_base_name = NULL;
  g_autoptr(GList) file_formats = NULL;
  const gchar *file_extension;
  g_autoptr(GFileInputStream) file_stream = NULL;
  guint8 *buffer = NULL;
  gsize buffer_size, bytes_read;
  GList *iter;
  gboolean status;

  g_assert (GEON_IS_WORLD (self));
  g_assert (world_file != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_world_get_instance_private (self);

  g_assert (G_IS_FILE (world_file->file));
  g_assert (world_file->content_type != NULL);
  g_assert (world_file->size > 0);

  world_file->format = NULL;

  file_base_name = g_file_get_basename (world_file->file);
  file_extension = g_strrstr (file_base_name, ".");

  file_formats = geon_file_formats_get_matches (world_file->content_type,
                                                file_extension);
  if (file_formats == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Given file format is not supported");
      return FALSE;
    }

  file_stream = g_file_read (world_file->file,
                             cancellable,
                             error);
  if (file_stream == NULL)
    return FALSE;

  buffer_size = HEADER_MAX_REQUEST_SIZE;
  buffer = g_malloc (buffer_size * sizeof (guint8));

  g_assert (buffer != NULL);

  status = g_input_stream_read_all (G_INPUT_STREAM (file_stream),
                                    buffer, buffer_size,
                                    &bytes_read,
                                    cancellable,
                                    error);
  if (status == FALSE)
    return FALSE;

  g_input_stream_close (G_INPUT_STREAM (file_stream),
                        cancellable, NULL);

  for (iter = file_formats; iter != NULL; iter = iter->next)
    {
      GeonFileFormat *file_format = iter->data;
      GeonFileAnalyzer *analyzer;
      g_autoptr(GBytes) header = NULL;

      analyzer = geon_application_get_analyzer_for_format (file_format);
      if (analyzer == NULL)
        continue;

      header = g_bytes_new (buffer, bytes_read);

      g_assert (header != NULL);

      status = geon_file_analyzer_validate_header (analyzer,
                                                   file_format,
                                                   world_file->size,
                                                   header);
      if (status == FALSE)
        continue;

      g_info ("Matching file '%s' with '%s' format",
              file_base_name, geon_file_format_get_name (file_format));

      world_file->format = file_format;

      return TRUE;
    }

  g_set_error (error,
               G_IO_ERROR,
               G_IO_ERROR_NOT_SUPPORTED,
               "Given file doesn't match any supported format");

  return FALSE;
}

static GeonWorldTree *
geon_world_do_load_instance_reference_tree (GeonWorld       *self,
                                            GFile           *file,
                                            GeonFileFormat  *file_format,
                                            GCancellable    *cancellable,
                                            GError         **error)
{
  GeonWorldPrivate *priv;
  GeonWorldTree *world_tree;
  g_autoptr(GeonTreeReference) root_reference = NULL;
  g_autoptr(GeonFileLoader) loader = NULL;
  gboolean status;

  g_assert (GEON_IS_WORLD (self));
  g_assert (G_IS_FILE (file));
  g_assert (file_format != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_world_get_instance_private (self);

  loader = geon_application_new_loader_for_format (file_format);

  if (loader == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Given model format is not supported");
      return NULL;
    }

  status = geon_file_loader_initialize (loader,
                                        file_format, file,
                                        cancellable,
                                        error);
  if (status == FALSE)
    return NULL;

  root_reference = geon_file_loader_load_tree (loader,
                                               cancellable,
                                               error);
  if (root_reference == NULL)
    return NULL;

  world_tree = geon_world_tree_new ();

  g_assert (world_tree != NULL);

  world_tree->loader = g_steal_pointer(&loader);
  world_tree->reference = g_steal_pointer (&root_reference);

  world_tree->tree = NULL;

  return world_tree;
}

static gboolean
geon_world_do_load_occurrence_tree (GeonWorld      *self,
                                    GeonWorldTree  *world_tree,
                                    GCancellable   *cancellable,
                                    GError        **error)
{
  GeonWorldPrivate *priv;
  g_autoptr(GtkTreeModel) root_tree = NULL;

  g_assert (GEON_IS_WORLD (self));
  g_assert (world_tree != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_world_get_instance_private (self);

  g_assert (GEON_IS_FILE_LOADER (world_tree->loader));
  g_assert (GEON_IS_TREE_REFERENCE (world_tree->reference));

  world_tree->tree = NULL;

  root_tree = geon_tree_new (world_tree->reference);

  if (root_tree == NULL)
    {
      g_set_error (error,
                   G_IO_ERROR,
                   G_IO_ERROR_NOT_SUPPORTED,
                   "Given model format is not supported");
      return FALSE;
    }

  world_tree->tree = g_steal_pointer (&root_tree);

  return TRUE;
}

static gboolean
geon_world_do_load_model_geometry (GeonWorld       *self,
                                   GeonFileLoader  *loader,
                                   GCancellable    *cancellable,
                                   GError         **error)
{
  GeonWorldPrivate *priv;
  gboolean status;

  g_assert (GEON_IS_WORLD (self));
  g_assert (GEON_IS_FILE_LOADER (loader));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_world_get_instance_private (self);

  status = geon_file_loader_load_geometry (loader,
                                           cancellable,
                                           error);

  return status;
}

static void
geon_world_set_file (GeonWorld *self,
                     GFile     *file)
{
  GeonWorldPrivate *priv;

  g_assert (GEON_IS_WORLD (self));
  g_assert (G_IS_FILE (file));

  priv = geon_world_get_instance_private (self);

  priv->file = g_file_dup (file);
}

static void
geon_world_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  GeonWorld *self = GEON_WORLD (object);

  g_return_if_fail (GEON_IS_WORLD (self));

  switch (property_id)
    {
    case PROP_FILE:
      g_value_set_object (value, geon_world_get_file (self));
      break;

    case PROP_TREE:
      g_value_set_object (value, geon_world_get_root_tree (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_world_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  GeonWorld *self = GEON_WORLD (object);

  g_return_if_fail (GEON_IS_WORLD (self));

  switch (property_id)
    {
    case PROP_FILE:
      geon_world_set_file (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_world_finalize (GObject *object)
{
  GeonWorld *self = GEON_WORLD (object);
  GeonWorldPrivate *priv;

  g_return_if_fail (GEON_IS_WORLD (self));

  priv = geon_world_get_instance_private (self);

  g_clear_object (&priv->root_tree);
  g_clear_object (&priv->root_reference);

  g_clear_object (&priv->file_loader);

  priv->file_format = NULL;

  g_clear_pointer (&priv->file_name, g_free);

  g_clear_object (&priv->file);

  G_OBJECT_CLASS (geon_world_parent_class)->finalize (object);
}

static void
geon_world_class_init (GeonWorldClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = geon_world_get_property;
  object_class->set_property = geon_world_set_property;
  object_class->finalize = geon_world_finalize;

  properties[PROP_FILE] =
    g_param_spec_object ("file",
                         "File",
                         "The 3D model source file",
                         G_TYPE_FILE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  properties[PROP_TREE] =
    g_param_spec_object ("tree",
                         "Tree",
                         "The tree stored representation of the model's assembly structure",
                         GTK_TYPE_TREE_MODEL,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
geon_world_init (GeonWorld *self)
{
}

GeonWorld *
geon_world_new_for_file (GFile   *file,
                         GError **error)
{
  GEON_ENTER;

  g_return_val_if_fail (G_IS_FILE (file), NULL);

  GEON_RETURN_VAL (g_initable_new (GEON_TYPE_WORLD,
                                   NULL, error,
                                   "file", file,
                                   NULL));
}

static void
geon_world_new_for_file_async_cb (GObject      *source_object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
  GAsyncInitable *initable = (GAsyncInitable *)source_object;
  GTask *task = (GTask *) user_data;
  g_autoptr(GObject) object = NULL;
  g_autoptr(GError) error = NULL;

  g_return_if_fail (G_IS_ASYNC_INITABLE (initable));
  g_return_if_fail (G_IS_TASK (task));

  object = g_async_initable_new_finish (initable, result, &error);

  if (object == NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_pointer (task, g_steal_pointer (&object), g_object_unref);
}

void
geon_world_new_for_file_async (GFile               *file,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_autoptr(GTask) task = NULL;

  GEON_ENTER;

  g_return_if_fail (G_IS_FILE (file));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  task = g_task_new (NULL, cancellable, callback, user_data);

  g_task_set_source_tag (task, geon_world_new_for_file_async);
  g_task_set_priority (task, G_PRIORITY_DEFAULT);

  g_async_initable_new_async (GEON_TYPE_WORLD,
                              G_PRIORITY_DEFAULT,
                              cancellable,
                              geon_world_new_for_file_async_cb,
                              g_steal_pointer (&task),
                              "file", file,
                              NULL);

  GEON_RETURN;
}

GeonWorld *
geon_world_new_for_file_finish   (GAsyncResult  *result,
                                  GError       **error)
{
  GTask *task = G_TASK (result);

  GEON_ENTER;

  g_return_val_if_fail (G_IS_TASK (task), NULL);

  GEON_RETURN_VAL (g_task_propagate_pointer (task, error));
}

GeonWorld *
geon_world_new_for_uri (const gchar  *uri,
                        GError      **error)
{
  g_autoptr(GFile) file = NULL;

  GEON_ENTER;

  g_return_val_if_fail (uri != NULL, NULL);

  file = g_file_new_for_uri (uri);

  GEON_RETURN_VAL (geon_world_new_for_file (file, error));
}

void
geon_world_new_for_uri_async (const gchar         *uri,
                              GCancellable        *cancellable,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data)
{
  g_autoptr(GFile) file = NULL;

  GEON_ENTER;

  g_return_if_fail (uri != NULL);

  file = g_file_new_for_uri (uri);

  geon_world_new_for_file_async (file,
                                 cancellable,
                                 callback,
                                 user_data);

  GEON_RETURN;
}

GeonWorld *
geon_world_new_for_uri_finish (GAsyncResult  *result,
                               GError       **error)
{
  GTask *task = G_TASK (result);

  GEON_ENTER;

  g_return_val_if_fail (G_IS_TASK (task), NULL);

  GEON_RETURN_VAL (g_task_propagate_pointer (task, error));
}

gboolean
geon_world_load_model_tree (GeonWorld     *self,
                            GCancellable  *cancellable,
                            GError       **error)
{
  GeonWorldPrivate *priv;
  GeonWorldTree *world_tree;
  gboolean status;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_WORLD (self), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  priv = geon_world_get_instance_private (self);

  if (priv->file == NULL || priv->file_format == NULL)
    GEON_RETURN_VAL (FALSE);

  world_tree = geon_world_do_load_instance_reference_tree (self,
                                                           priv->file,
                                                           priv->file_format,
                                                           cancellable,
                                                           error);
  if (world_tree == NULL)
    GEON_RETURN_VAL (FALSE);

  status = geon_world_do_load_occurrence_tree (self,
                                               world_tree,
                                               cancellable,
                                               error);
  if (status == FALSE)
    {
      geon_world_tree_free (world_tree);
      GEON_RETURN_VAL (FALSE);
    }

  priv->file_loader = g_object_ref (world_tree->loader);
  priv->root_reference = g_object_ref (world_tree->reference);
  priv->root_tree = g_object_ref (world_tree->tree);

  geon_world_tree_free (world_tree);

  GEON_RETURN_VAL (TRUE);
}

static void
geon_world_load_model_tree_worker (GTask        *task,
                                   gpointer      source_object,
                                   gpointer      task_data,
                                   GCancellable *cancellable)
{
  GeonWorld *self = source_object;
  GeonWorldFile *world_file = task_data;
  GeonWorldTree *world_tree;
  gboolean status;
  GError *error = NULL;

  GEON_ENTER;

  g_assert (G_IS_TASK (task));
  g_assert (GEON_IS_WORLD (self));
  g_assert (world_file != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  world_tree = geon_world_do_load_instance_reference_tree (self,
                                                           world_file->file,
                                                           world_file->format,
                                                           cancellable,
                                                           &error);

  g_task_set_task_data (task, NULL, NULL);

  if (world_tree == NULL)
    {
      g_task_return_error (task, error);
      GEON_RETURN;
    }

  status = geon_world_do_load_occurrence_tree (self,
                                               world_tree,
                                               cancellable,
                                               &error);
  if (status == FALSE)
    {
      geon_world_tree_free (world_tree);
      g_task_return_error (task, error);
      GEON_RETURN;
    }

  g_task_set_task_data (task, world_tree, geon_world_tree_free);

  g_task_return_boolean (task, TRUE);

  GEON_RETURN;
}

void
geon_world_load_model_tree_async (GeonWorld           *self,
                                  GCancellable        *cancellable,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
  GeonWorldPrivate *priv;
  g_autoptr(GTask) task = NULL;
  GeonWorldFile *world_file;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_WORLD (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_world_get_instance_private (self);

  task = g_task_new (self, cancellable, callback, user_data);

  world_file = geon_world_file_new (priv->file);

  world_file->name = g_strdup (priv->file_name);
  world_file->writable = priv->file_is_writable;
  world_file->size = priv->file_size;
  world_file->format = priv->file_format;

  g_task_set_source_tag (task, geon_world_load_model_tree_async);
  g_task_set_task_data (task, world_file, geon_world_file_free);
  g_task_set_priority (task, G_PRIORITY_HIGH);

  g_task_run_in_thread (g_steal_pointer (&task),
                        geon_world_load_model_tree_worker);

  GEON_RETURN;
}

gboolean
geon_world_load_model_tree_finish (GeonWorld     *self,
                                   GAsyncResult  *result,
                                   GError       **error)
{
  GeonWorldPrivate *priv;
  GTask *task = G_TASK (result);
  GeonWorldTree *world_tree;
  gboolean status;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_WORLD (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (task), FALSE);
  g_return_val_if_fail (g_task_is_valid (task, self), FALSE);

  priv = geon_world_get_instance_private (self);

  status = g_task_propagate_boolean (task, error);
  if (status == FALSE)
    GEON_RETURN_VAL (FALSE);

  world_tree = g_task_get_task_data (task);

  if (world_tree != NULL)
    {
      priv->file_loader = g_object_ref (world_tree->loader);
      priv->root_reference = g_object_ref (world_tree->reference);
      priv->root_tree = g_object_ref (world_tree->tree);
    }

  GEON_RETURN_VAL (TRUE);
}

gboolean
geon_world_load_model_geometry (GeonWorld     *self,
                                GCancellable  *cancellable,
                                GError       **error)
{
  GeonWorldPrivate *priv;
  gboolean status;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_WORLD (self), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  priv = geon_world_get_instance_private (self);

  if (priv->file_loader == NULL)
    GEON_RETURN_VAL (FALSE);

  status = geon_world_do_load_model_geometry (self,
                                              priv->file_loader,
                                              cancellable,
                                              error);

  GEON_RETURN_VAL (status);
}

static void
geon_world_load_model_geometry_worker (GTask        *task,
                                       gpointer      source_object,
                                       gpointer      task_data,
                                       GCancellable *cancellable)
{
  GeonWorld *self = source_object;
  GeonWorldTree *world_tree = task_data;
  gboolean status;
  GError *error = NULL;

  GEON_ENTER;

  g_assert (G_IS_TASK (task));
  g_assert (GEON_IS_WORLD (self));
  g_assert (world_tree != NULL);
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  status = geon_world_do_load_model_geometry (self,
                                              world_tree->loader,
                                              cancellable,
                                              &error);
  if (status == FALSE)
    {
      g_task_return_error (task, error);
      GEON_RETURN;
    }

  g_task_return_boolean (task, TRUE);

  GEON_RETURN;
}

void
geon_world_load_model_geometry_async (GeonWorld           *self,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  GeonWorldPrivate *priv;
  g_autoptr(GTask) task = NULL;
  GeonWorldTree *world_tree;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_WORLD (self));
  g_return_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable));

  priv = geon_world_get_instance_private (self);

  task = g_task_new (self, cancellable, callback, user_data);

  world_tree = geon_world_tree_new ();

  world_tree->loader = g_object_ref (priv->file_loader);

  g_task_set_source_tag (task, geon_world_load_model_geometry_async);
  g_task_set_task_data (task, world_tree, geon_world_tree_free);
  g_task_set_priority (task, G_PRIORITY_HIGH);

  g_task_run_in_thread (g_steal_pointer (&task),
                        geon_world_load_model_geometry_worker);

  GEON_RETURN;
}

gboolean
geon_world_load_model_geometry_finish (GeonWorld     *self,
                                       GAsyncResult  *result,
                                       GError       **error)
{
  GeonWorldPrivate *priv;
  GTask *task = G_TASK (result);
  gboolean status;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_WORLD (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (task), FALSE);
  g_return_val_if_fail (g_task_is_valid (task, self), FALSE);

  priv = geon_world_get_instance_private (self);

  status = g_task_propagate_boolean (task, error);
  if (status == FALSE)
    GEON_RETURN_VAL (FALSE);

  GEON_RETURN_VAL (TRUE);
}

GFile *
geon_world_get_file (GeonWorld *self)
{
  GeonWorldPrivate *priv;

  g_return_val_if_fail (GEON_IS_WORLD (self), NULL);

  priv = geon_world_get_instance_private (self);

  return priv->file;
}

gboolean
geon_world_is_assembly (GeonWorld *self)
{
  GeonWorldPrivate *priv;
  g_autoptr (GtkTreePath) first_child_path = NULL;
  GtkTreeIter iter;

  g_return_val_if_fail (GEON_IS_WORLD (self), FALSE);

  priv = geon_world_get_instance_private (self);

  if (priv->root_tree == NULL)
    return FALSE;

  first_child_path = gtk_tree_path_new_from_indices (1, -1);

  return gtk_tree_model_get_iter (priv->root_tree, &iter,
                                  first_child_path);
}

GtkTreeModel *
geon_world_get_root_tree (GeonWorld *self)
{
  GeonWorldPrivate *priv;

  g_return_val_if_fail (GEON_IS_WORLD (self), NULL);

  priv = geon_world_get_instance_private (self);

  return priv->root_tree;
}

static gboolean
geon_world_initable_init (GInitable     *initable,
                          GCancellable  *cancellable,
                          GError       **error)
{
  GeonWorld *self = GEON_WORLD (initable);
  GeonWorldPrivate *priv;
  GeonWorldFile *world_file;
  gboolean status;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_WORLD (self), FALSE);
  g_return_val_if_fail (!cancellable || G_IS_CANCELLABLE (cancellable), FALSE);

  priv = geon_world_get_instance_private (self);

  g_assert (G_IS_FILE (priv->file));

  world_file = geon_world_do_query_file_info (self,
                                             priv->file,
                                             NULL,
                                             error);
  if (world_file == NULL)
    GEON_RETURN_VAL (FALSE);

  status = geon_world_do_analyze_file (self,
                                       world_file,
                                       NULL,
                                       error);
  if (status == FALSE)
    {
      geon_world_file_free (world_file);
      GEON_RETURN_VAL (FALSE);
    }

  priv->file_name = g_strdup (world_file->name);
  priv->file_is_writable = world_file->writable;
  priv->file_size = world_file->size;
  priv->file_format = world_file->format;

  geon_world_file_free (world_file);

  GEON_RETURN_VAL (TRUE);
}

static void
geon_world_initable_iface_init (GInitableIface *iface)
{
  iface->init = geon_world_initable_init;
}

static void
geon_world_async_initable_init_worker (GTask        *task,
                                       gpointer      source_object,
                                       gpointer      task_data,
                                       GCancellable *cancellable)
{
  GeonWorld *self = source_object;
  GFile *file = task_data;
  GeonWorldFile *world_file;
  gboolean status;
  GError *error = NULL;

  GEON_ENTER;

  g_assert (G_IS_TASK (task));
  g_assert (GEON_IS_WORLD (self));
  g_assert (G_IS_FILE (file));
  g_assert (!cancellable || G_IS_CANCELLABLE (cancellable));

  world_file = geon_world_do_query_file_info (self,
                                             file,
                                             cancellable,
                                             &error);
  if (world_file == NULL)
    {
      g_task_set_task_data (task, NULL, NULL);
      g_task_return_error (task, error);
      GEON_RETURN;
    }

  status = geon_world_do_analyze_file (self,
                                       world_file,
                                       cancellable,
                                       &error);
  if (status == FALSE)
    {
      geon_world_file_free (world_file);
      g_task_set_task_data (task, NULL, NULL);
      g_task_return_error (task, error);
      GEON_RETURN;
    }

  g_task_set_task_data (task, world_file, geon_world_file_free);

  g_task_return_boolean (task, TRUE);

  GEON_RETURN;
}

static void
geon_world_async_initable_init_async (GAsyncInitable      *initable,
                                      gint                 io_priority,
                                      GCancellable        *cancellable,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
  GeonWorld *self = GEON_WORLD (initable);
  GeonWorldPrivate *priv;
  g_autoptr(GTask) task = NULL;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_WORLD (self));

  priv = geon_world_get_instance_private (self);

  g_assert (G_IS_FILE (priv->file));

  task = g_task_new (self, cancellable, callback, user_data);

  g_task_set_source_tag (task, geon_world_async_initable_init_async);
  g_task_set_task_data (task, g_file_dup (priv->file), g_object_unref);
  g_task_set_priority (task, G_PRIORITY_HIGH);

  g_task_run_in_thread (g_steal_pointer (&task),
                        geon_world_async_initable_init_worker);

  GEON_RETURN;
}

static gboolean
geon_world_async_initable_init_finish (GAsyncInitable  *initable,
                                       GAsyncResult    *result,
                                       GError         **error)
{
  GeonWorld *self = GEON_WORLD (initable);
  GeonWorldPrivate *priv;
  GTask *task = G_TASK (result);
  GeonWorldFile *world_file;
  gboolean status;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_WORLD (self), FALSE);
  g_return_val_if_fail (G_IS_TASK (task), FALSE);
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);

  priv = geon_world_get_instance_private (self);

  status = g_task_propagate_boolean (task, error);
  if (status == FALSE)
      GEON_RETURN_VAL (FALSE);

  world_file = g_task_get_task_data (task);

  if (world_file != NULL)
    {
      priv->file_name = g_strdup (world_file->name);
      priv->file_is_writable = world_file->writable;
      priv->file_size = world_file->size;
      priv->file_format = world_file->format;
    }

  GEON_RETURN_VAL (TRUE);
}

static void
geon_world_async_initable_iface_init (GAsyncInitableIface *iface)
{
  iface->init_async = geon_world_async_initable_init_async;
  iface->init_finish = geon_world_async_initable_init_finish;
}

/* ex:set ts=2 sw=2 et: */
