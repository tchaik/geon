/* This file is part of Geon: geon-file-loader.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

#include <gio/gio.h>

#include "geon-file-formats.h"
#include "geon-tree-reference.h"

G_BEGIN_DECLS

#define GEON_TYPE_FILE_LOADER (geon_file_loader_get_type ())

G_DECLARE_INTERFACE (GeonFileLoader, geon_file_loader,
                     GEON, FILE_LOADER,
                     GObject)

struct _GeonFileLoaderInterface
{
  GTypeInterface parent;

  gboolean             (*initialize)       (GeonFileLoader  *self,
                                            GeonFileFormat  *file_format,
                                            GFile           *file,
                                            GCancellable    *cancellable,
                                            GError         **error);

  GeonTreeReference   *(*load_tree)        (GeonFileLoader  *self,
                                            GCancellable    *cancellable,
                                            GError         **error);

  gboolean             (*load_geonmetry)   (GeonFileLoader  *self,
                                            GCancellable    *cancellable,
                                            GError         **error);
};

gboolean             geon_file_loader_initialize      (GeonFileLoader  *self,
                                                       GeonFileFormat  *file_format,
                                                       GFile           *file,
                                                       GCancellable    *cancellable,
                                                       GError         **error);

GeonTreeReference   *geon_file_loader_load_tree       (GeonFileLoader  *self,
                                                       GCancellable    *cancellable,
                                                       GError         **error);

gboolean             geon_file_loader_load_geometry   (GeonFileLoader  *self,
                                                       GCancellable    *cancellable,
                                                       GError         **error);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
