/* This file is part of Geon: geon-stl-file-analyzer.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-STLFileAnalyzer"

#include <glib/gi18n.h>
#include <glib-object.h>

#include <libpeas/peas.h>

#include "geon-debug.h"
#include "geon-file-analyzer.h"
#include "geon-file-formats.h"

typedef struct
{
  GPtrArray *formats;

  gsize    ascii_header_size;
  gsize    binary_header_size;
} GeonSTLFileAnalyzerPrivate;

struct _GeonSTLFileAnalyzer
{
  GObject parent_instance;
};

#define GEON_TYPE_STL_FILE_ANALYZER (geon_stl_file_analyzer_get_type ())

G_DECLARE_FINAL_TYPE (GeonSTLFileAnalyzer, geon_stl_file_analyzer,
                      GEON, STL_FILE_ANALYZER,
                      GObject)

static void geon_stl_file_analyzer_iface_init (GeonFileAnalyzerInterface *iface);

G_DEFINE_TYPE_EXTENDED (GeonSTLFileAnalyzer, geon_stl_file_analyzer,
                        G_TYPE_OBJECT, 0,
                        G_IMPLEMENT_INTERFACE (GEON_TYPE_FILE_ANALYZER,
                                               geon_stl_file_analyzer_iface_init)
                        G_ADD_PRIVATE (GeonSTLFileAnalyzer))

#define ASCII_MIN_FILE_SIZE 15
#define ASCII_SIZE_REQUEST  4096

#define ASCII_SOLID_BLOCK_START "solid"
#define ASCII_SOLID_BLOCK_END   "endsolid"
#define ASCII_FACET_BLOCK_START "facet"
#define ASCII_FACET_BLOCK_END   "endfacet"

#define BINARY_MIN_FILE_SIZE 84
#define BINARY_SIZE_REQUEST  84

#define BINARY_HEADER_SIZE 80
#define BINARY_BLOCK_SIZE  50

static void
geon_stl_file_analyzer_class_init (GeonSTLFileAnalyzerClass *klass)
{
}

static void
geon_stl_file_analyzer_init (GeonSTLFileAnalyzer *self)
{
  GeonSTLFileAnalyzerPrivate *priv;
  GeonFileFormat *format;

  priv = geon_stl_file_analyzer_get_instance_private (self);

  priv->formats = g_ptr_array_sized_new (2);

  format = geon_file_format_new ("STL", "STL-ASCII");

  geon_file_format_add_file_extension (format, ".stl");
  geon_file_format_add_content_type (format, "model/x.stl-ascii");
  geon_file_format_add_content_type (format, "model/stl");

  priv->ascii_header_size = ASCII_SIZE_REQUEST;

  g_ptr_array_add (priv->formats, format);

  format = geon_file_format_new ("STL", "STL-binary");

  geon_file_format_add_file_extension (format, ".stl");
  geon_file_format_add_content_type (format, "model/x.stl-binary");
  geon_file_format_add_content_type (format, "model/stl");

  priv->binary_header_size = BINARY_SIZE_REQUEST;

  g_ptr_array_add (priv->formats, format);
}

static void
geon_stl_file_analyzer_register_file_formats (GeonFileAnalyzer *analyzer)
{
  GeonSTLFileAnalyzer *self = GEON_STL_FILE_ANALYZER (analyzer);
  GeonSTLFileAnalyzerPrivate *priv;
  guint i;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_STL_FILE_ANALYZER (self));

  priv = geon_stl_file_analyzer_get_instance_private (self);

  for (i = 0; i < priv->formats->len; i++)
  {
    GeonFileFormat *format = g_ptr_array_index (priv->formats, i);

    format = geon_file_format_copy (format);

    g_assert (format != NULL);

    geon_file_formats_register (format);
  }

  GEON_RETURN;
}

static gsize
geon_stl_file_analyzer_get_header_size (GeonFileAnalyzer *analyzer,
                                        GeonFileFormat   *file_format)
{
  GeonSTLFileAnalyzer *self = GEON_STL_FILE_ANALYZER (analyzer);
  GeonSTLFileAnalyzerPrivate *priv;
  const gchar *format_name;

  g_return_val_if_fail (GEON_IS_STL_FILE_ANALYZER (self), 0);
  g_return_val_if_fail (file_format != NULL, 0);

  priv = geon_stl_file_analyzer_get_instance_private (self);

  format_name = geon_file_format_get_name (file_format);

  if (g_strcmp0 (format_name, "STL-ASCII") == 0)
    return priv->ascii_header_size;

  if (g_strcmp0 (format_name, "STL-binary") == 0)
    return priv->binary_header_size;

  return 0;
}

static gboolean
geon_stl_file_analyzer_validate_ascii_header (GDataInputStream *data_stream,
                                              guint64           file_size)
{
  g_autofree gchar *first_line = NULL;
  g_autofree gchar *second_line = NULL;
  gchar *haystack, *match;
  g_autofree gchar *needle = NULL;
  gsize lenght;
  g_autoptr(GError) error = NULL;

  g_assert (G_IS_DATA_INPUT_STREAM (data_stream));
  g_assert (file_size > 0);

  if (file_size < ASCII_MIN_FILE_SIZE)
    return FALSE;

  first_line = g_data_input_stream_read_line (data_stream,
                                              NULL, NULL, NULL);
  if (first_line == NULL)
    return FALSE;
  else if (!g_str_is_ascii (first_line))
    return FALSE;

  if (!g_str_has_prefix (first_line, ASCII_SOLID_BLOCK_START))
    return FALSE;

  second_line = g_data_input_stream_read_line (data_stream,
                                               NULL, NULL, NULL);
  if (second_line == NULL && error != NULL)
    return FALSE;
  else if (second_line != NULL)
    {
      haystack = g_strstrip (second_line);
      lenght = MIN (strlen (haystack), strlen (ASCII_FACET_BLOCK_START));
      needle = g_strndup (ASCII_FACET_BLOCK_START, lenght);

      match = g_strstr_len (haystack, lenght, needle);

      if (haystack != match)
        return FALSE;
    }

  return TRUE;
}

static gboolean
geon_stl_file_analyzer_validate_binary_header (GDataInputStream *data_stream,
                                               guint64           file_size)
{
  guint64 expected_size = BINARY_MIN_FILE_SIZE;
  guint32 nb_facet;
  g_autoptr(GError) error = NULL;
  gboolean status;

  g_assert (G_IS_DATA_INPUT_STREAM (data_stream));
  g_assert (file_size > 0);

  if (file_size < BINARY_MIN_FILE_SIZE)
    return FALSE;

  status = g_seekable_seek (G_SEEKABLE (data_stream),
                            BINARY_HEADER_SIZE, G_SEEK_SET,
                            NULL, &error);
  if (status == FALSE || error != NULL)
    return FALSE;

  nb_facet = g_data_input_stream_read_uint32 (data_stream,
                                              NULL, &error);
  if (error != NULL)
    return FALSE;

  expected_size += nb_facet * BINARY_BLOCK_SIZE;
  if (file_size != expected_size)
    return FALSE;

  return TRUE;
}

static gboolean
geon_stl_file_analyzer_validate_header (GeonFileAnalyzer *analyzer,
                                        GeonFileFormat   *file_format,
                                        guint64           file_size,
                                        GBytes            *header)
{
  GeonSTLFileAnalyzer *self = GEON_STL_FILE_ANALYZER (analyzer);
  GeonSTLFileAnalyzerPrivate *priv;
  g_autoptr(GInputStream) input_stream = NULL;
  g_autoptr(GDataInputStream) data_stream = NULL;
  const gchar *format_name;

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_STL_FILE_ANALYZER (self), FALSE);
  g_return_val_if_fail (file_format != NULL, FALSE);
  g_return_val_if_fail (file_size > 0, FALSE);
  g_return_val_if_fail (header != NULL, FALSE);

  priv = geon_stl_file_analyzer_get_instance_private (self);

  input_stream = g_memory_input_stream_new_from_bytes (header);

  g_assert (input_stream != NULL);

  data_stream = g_data_input_stream_new (input_stream);

  g_assert (G_IS_DATA_INPUT_STREAM (data_stream));

  g_data_input_stream_set_byte_order (data_stream,
                                      G_DATA_STREAM_BYTE_ORDER_LITTLE_ENDIAN);

  format_name = geon_file_format_get_name (file_format);

  if (g_strcmp0 (format_name, "STL-ASCII") == 0
   && g_bytes_get_size (header) >= ASCII_MIN_FILE_SIZE)
    {
      GEON_RETURN_VAL (geon_stl_file_analyzer_validate_ascii_header (data_stream,
                                                                     file_size));
    }

  if (g_strcmp0 (format_name, "STL-binary") == 0
   && g_bytes_get_size (header) >= BINARY_MIN_FILE_SIZE)
    {
      GEON_RETURN_VAL (geon_stl_file_analyzer_validate_binary_header (data_stream,
                                                                      file_size));
    }

  GEON_RETURN_VAL (FALSE);
}

static void
geon_stl_file_analyzer_iface_init (GeonFileAnalyzerInterface *iface)
{
  iface->register_file_formats = geon_stl_file_analyzer_register_file_formats;
  iface->get_header_size = geon_stl_file_analyzer_get_header_size;
  iface->validate_header = geon_stl_file_analyzer_validate_header;
}

extern void
geon_stl_file_analyzer_register_types (PeasObjectModule *module)
{
  peas_object_module_register_extension_type (module,
                                              GEON_TYPE_FILE_ANALYZER,
                                              GEON_TYPE_STL_FILE_ANALYZER);
}

/* ex:set ts=2 sw=2 et: */
