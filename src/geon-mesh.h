/* This file is part of Geon: geon-mesh.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

typedef enum
{
  GEON_MESH_DIMENSION_UNKNOWN,
  GEON_MESH_DIMENSION_2D,
  GEON_MESH_DIMENSION_3D,
} GeonMeshDimension;

typedef enum
{
  GEON_MESH_PRECISION_UNKNOWN,
  GEON_MESH_PRECISION_SINGLE,
  GEON_MESH_PRECISION_DOUBLE,
} GeonMeshPrecision;

typedef enum
{
  GEON_MESH_ELEMENT_TYPE_UNKNOWN         = 0 << 0,
  GEON_MESH_ELEMENT_TYPE_SEGMENT         = 1 << 0,
  GEON_MESH_ELEMENT_TYPE_TRIANGLE        = 1 << 1,
  GEON_MESH_ELEMENT_TYPE_QUADRILATERAL   = 1 << 2,
  GEON_MESH_ELEMENT_TYPE_TETRAHEDRON     = 1 << 3,
  GEON_MESH_ELEMENT_TYPE_PYRAMID         = 1 << 4,
  GEON_MESH_ELEMENT_TYPE_PRISM           = 1 << 5,
  GEON_MESH_ELEMENT_TYPE_HEXAHEDRON      = 1 << 6,
} GeonMeshElementType;

#define GEON_TYPE_MESH (geon_mesh_get_type())

typedef struct _GeonMesh GeonMesh;

GeonMesh            *geon_mesh_new                 (GeonMeshDimension dimension,
                                                    GeonMeshPrecision precision);

GeonMesh            *geon_mesh_ref                 (GeonMesh *self);

void                 geon_mesh_unref               (GeonMesh *self);

GeonMeshDimension    geon_mesh_get_dimension       (GeonMesh *self);

GeonMeshPrecision    geon_mesh_get_precision       (GeonMesh *self);

guint                geon_mesh_allocate_vertices   (GeonMesh            *self,
                                                    GeonMeshElementType  element_type,
                                                    guint                nb_elements);

guint                geon_mesh_allocate_elements   (GeonMesh            *self,
                                                    GeonMeshElementType  element_type,
                                                    guint                nb_elements);

guint                geon_mesh_add_elements        (GeonMesh            *self,
                                                    GeonMeshElementType  element_type,
                                                    guint                nb_elements,
                                                    gconstpointer        vertex_array,
                                                    gsize                array_size);

guint                geon_mesh_add_elements_full   (GeonMesh            *self,
                                                    GeonMeshDimension    dimension,
                                                    GeonMeshPrecision    precision,
                                                    GeonMeshElementType  element_type,
                                                    guint                nb_elements,
                                                    gconstpointer        vertex_array,
                                                    gsize                array_size);

const GArray        *geon_mesh_get_vertex_array    (GeonMesh *self);

const GArray        *geon_mesh_get_index_array     (GeonMesh *self);

GeonMesh            *geon_mesh_copy                (const GeonMesh *self);

void                 geon_mesh_free                (GeonMesh *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GeonMesh, geon_mesh_free)

static guint   geon_mesh_nb_points_for_elements    (GeonMeshElementType element_type,
                                                    guint               nb_elements);

static guint   geon_mesh_array_size_for_elements   (GeonMeshDimension   dimension,
                                                    GeonMeshElementType element_type,
                                                    guint               nb_elements);

static gsize   geon_mesh_byte_size_for_elements    (GeonMeshDimension   dimension,
                                                    GeonMeshPrecision   precision,
                                                    GeonMeshElementType element_type,
                                                    guint               nb_elements);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
