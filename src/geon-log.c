/* This file is part of Geon: geon-log.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-Log"

#include "config.h"

#include <glib/gi18n.h>
#include <unistd.h>

#include "geon-debug.h"

#include "geon-log.h"

static GIOChannel *stdout_channel;
static gboolean use_colored_output;
static gint output_verbosity;

G_LOCK_DEFINE (write_lock);

static void
geon_log_handler_func (const gchar    *log_domain,
                       GLogLevelFlags  log_level,
                       const gchar    *message,
                       gpointer        user_data)
{
  g_autofree gchar *buffer = NULL;
  const gchar *level_string;
  gchar time_string[32];
  gint64 now;
  time_t seconds;
  glong fraction;

  if (G_LIKELY (stdout_channel != NULL))
    {
      switch ((gint)log_level)
        {
        case G_LOG_LEVEL_ERROR:
          if (use_colored_output)
            level_string = "   \033[1;31mERROR\033[0m";
          else
            level_string = "   ERROR";
          break;

        case G_LOG_LEVEL_CRITICAL:
          if (use_colored_output)
            level_string = "\033[1;35mCRITICAL\033[0m";
          else
            level_string = "CRITICAL";
          break;

        case G_LOG_LEVEL_WARNING:
          if (use_colored_output)
            level_string = " \033[1;33mWARNING\033[0m";
          else
            level_string = " WARNING";
          break;

        case G_LOG_LEVEL_MESSAGE:
          if (output_verbosity < 1)
            return;
          if (use_colored_output)
            level_string = " \033[1;32mMESSAGE\033[0m";
          else
            level_string = " MESSAGE";
          break;

        case G_LOG_LEVEL_INFO:
          if (output_verbosity < 2)
            return;
          if (use_colored_output)
            level_string = "    \033[1;32mINFO\033[0m";
          else
            level_string = "    INFO";
          break;

        case G_LOG_LEVEL_DEBUG:
          if (output_verbosity < 3)
            return;
          if (use_colored_output)
            level_string = "   \033[1;32mDEBUG\033[0m";
          else
            level_string = "   DEBUG";
          break;

        case G_LOG_LEVEL_TRACE:
          if (output_verbosity < 4)
            return;
          if (use_colored_output)
            level_string = "   \033[1;36mTRACE\033[0m";
          else
            level_string = "   TRACE";
          break;

        default:
          level_string = " UNKNOWN";
          break;
        }

      now = g_get_real_time ();
      seconds = now / G_USEC_PER_SEC;
      fraction = now % G_USEC_PER_SEC;

      strftime (time_string,
                sizeof (time_string),
                "%H:%M:%S", localtime (&seconds));

      buffer = g_strdup_printf ("%s.%04ld  %20s: %s: %s\n",
                                time_string, fraction,
                                log_domain,
                                level_string,
                                message);

      G_LOCK (write_lock);

      g_io_channel_write_chars (stdout_channel, buffer, -1, NULL, NULL);
      g_io_channel_flush (stdout_channel, NULL);

      G_UNLOCK (write_lock);
    }
}

void
geon_log_init (void)
{
  static gsize initialized = FALSE;

  if (g_once_init_enter (&initialized))
    {
      use_colored_output = isatty (STDOUT_FILENO);
      output_verbosity = 0;

      stdout_channel = g_io_channel_unix_new (STDOUT_FILENO);

      g_log_set_default_handler (geon_log_handler_func , NULL);

      g_once_init_leave (&initialized, TRUE);
    }
}

gint
geon_log_get_verbosity (void)
{
  return output_verbosity;
}

void
geon_log_increase_verbosity (void)
{
  output_verbosity++;
}

/* ex:set ts=2 sw=2 et: */
