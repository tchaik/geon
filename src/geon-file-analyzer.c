/* This file is part of Geon: geon-file-analyzer.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-FileAnalyzer"

#include "config.h"

#include <glib/gi18n.h>

#include "geon-debug.h"

#include "geon-file-analyzer.h"

G_DEFINE_INTERFACE (GeonFileAnalyzer, geon_file_analyzer,
                    G_TYPE_OBJECT)

static void
geon_file_analyzer_default_init (GeonFileAnalyzerInterface *iface)
{
  iface->register_file_formats = NULL;
  iface->get_header_size = NULL;
  iface->validate_header = NULL;
}

void
geon_file_analyzer_register_file_formats (GeonFileAnalyzer *self)
{
  g_return_if_fail (GEON_IS_FILE_ANALYZER (self));

  if (GEON_FILE_ANALYZER_GET_IFACE (self)->register_file_formats)
    GEON_FILE_ANALYZER_GET_IFACE (self)->register_file_formats (self);
}

gsize
geon_file_analyzer_get_header_size (GeonFileAnalyzer *self,
                                    GeonFileFormat   *file_format)
{
  g_return_val_if_fail (GEON_IS_FILE_ANALYZER (self), 0);
  g_return_val_if_fail (file_format != NULL, 0);

  if (GEON_FILE_ANALYZER_GET_IFACE (self)->get_header_size)
    return GEON_FILE_ANALYZER_GET_IFACE (self)->get_header_size (self, file_format);

  return 0;
}

gboolean
geon_file_analyzer_validate_header (GeonFileAnalyzer *self,
                                    GeonFileFormat   *file_format,
                                    guint64           file_size,
                                    GBytes            *header)
{
  g_return_val_if_fail (GEON_IS_FILE_ANALYZER (self), FALSE);
  g_return_val_if_fail (file_format != NULL, FALSE);
  g_return_val_if_fail (file_size > 0, FALSE);
  g_return_val_if_fail (header != NULL, FALSE);

  if (GEON_FILE_ANALYZER_GET_IFACE (self)->validate_header)
    return GEON_FILE_ANALYZER_GET_IFACE (self)->validate_header (self,
                                                                 file_format, file_size,
                                                                 header);

  return FALSE;
}

/* ex:set ts=2 sw=2 et: */
