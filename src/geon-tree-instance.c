/* This file is part of Geon: geon-tree-instance.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-TreeInstance"

#include "config.h"

#include <glib/gi18n.h>
#include <graphene.h>

#include "geon-debug.h"

#include "geon-tree-instance.h"

typedef struct
{
  gchar             *name;

  GeonTreeReference *reference;
  GeonTreeReference *parent;

  graphene_matrix_t *position;
} GeonTreeInstancePrivate;

struct _GeonTreeInstance
{
  GObject parent_instance;
};

enum {
  PROP_0,
  PROP_NAME,
  PROP_REFERENCE,
  N_PROPS
};

G_DEFINE_TYPE_WITH_PRIVATE (GeonTreeInstance, geon_tree_instance,
                            G_TYPE_OBJECT)

static GParamSpec *properties[N_PROPS];

static void
geon_tree_instance_set_reference (GeonTreeInstance  *self,
                                  GeonTreeReference *reference)
{
  GeonTreeInstancePrivate *priv;

  g_assert (GEON_IS_TREE_INSTANCE (self));
  g_assert (GEON_IS_TREE_REFERENCE (reference));

  priv = geon_tree_instance_get_instance_private (self);

  g_set_object (&priv->reference, reference);
}

static void
geon_tree_instance_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  GeonTreeInstance *self = GEON_TREE_INSTANCE (object);

  g_return_if_fail (GEON_IS_TREE_INSTANCE (self));

  switch (property_id)
    {
    case PROP_NAME:
      g_value_set_string (value, geon_tree_instance_get_name (self));
      break;

    case PROP_REFERENCE:
      g_value_set_object (value, geon_tree_instance_get_reference (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_tree_instance_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  GeonTreeInstance *self = GEON_TREE_INSTANCE (object);

  g_return_if_fail (GEON_IS_TREE_INSTANCE (self));

  switch (property_id)
    {
    case PROP_NAME:
      geon_tree_instance_set_name (self, g_value_get_string (value));
      break;

    case PROP_REFERENCE:
      geon_tree_instance_set_reference (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_tree_instance_finalize (GObject *object)
{
  GeonTreeInstance *self = GEON_TREE_INSTANCE (object);
  GeonTreeInstancePrivate *priv;

  g_return_if_fail (GEON_IS_TREE_INSTANCE (self));

  priv = geon_tree_instance_get_instance_private (self);

  graphene_matrix_free (priv->position);

  g_clear_object (&priv->reference);

  g_free (priv->name);

  G_OBJECT_CLASS (geon_tree_instance_parent_class)->finalize (object);
}

static void
geon_tree_instance_class_init (GeonTreeInstanceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = geon_tree_instance_get_property;
  object_class->set_property = geon_tree_instance_set_property;
  object_class->finalize = geon_tree_instance_finalize;

  properties[PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The instance name",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  properties[PROP_REFERENCE] =
    g_param_spec_object ("reference",
                         "Reference",
                         "The pointed tree reference",
                         GEON_TYPE_TREE_REFERENCE,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
geon_tree_instance_init (GeonTreeInstance *self)
{
}

GeonTreeInstance *
geon_tree_instance_new (const gchar       *name,
                        GeonTreeReference *reference)
{
  g_return_val_if_fail (name != NULL, NULL);
  g_return_val_if_fail (GEON_IS_TREE_REFERENCE (reference), NULL);

  return g_object_new (GEON_TYPE_TREE_INSTANCE,
                       "name", name,
                       "reference", reference,
                       NULL);
}

const gchar *
geon_tree_instance_get_name (GeonTreeInstance *self)
{
  GeonTreeInstancePrivate *priv;

  g_return_val_if_fail (GEON_IS_TREE_INSTANCE (self), NULL);

  priv = geon_tree_instance_get_instance_private (self);

  return g_strdup (priv->name);
}

void
geon_tree_instance_set_name (GeonTreeInstance *self,
                             const gchar      *name)
{
  GeonTreeInstancePrivate *priv;

  g_return_if_fail (GEON_IS_TREE_INSTANCE (self));
  g_return_if_fail (name != NULL);

  priv = geon_tree_instance_get_instance_private (self);

  if (!g_utf8_validate (name, -1, NULL))
    priv->name = g_utf8_make_valid (name, -1);
  else
    priv->name = g_strdup (name);

  g_object_notify_by_pspec (G_OBJECT (self),
                            properties[PROP_NAME]);
}

GeonTreeReference *
geon_tree_instance_get_reference (GeonTreeInstance *self)
{
  GeonTreeInstancePrivate *priv;

  g_return_val_if_fail (GEON_IS_TREE_INSTANCE (self), NULL);

  priv = geon_tree_instance_get_instance_private (self);

  return priv->reference;
}

/* ex:set ts=2 sw=2 et: */
