/* This file is part of Geon: geon.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define __GEON_INSIDE__

#include "geon-application.h"
#include "geon-file-analyzer.h"
#include "geon-file-formats.h"
#include "geon-file-loader.h"
#include "geon-log.h"
#include "geon-mesh.h"
#include "geon-tree.h"
#include "geon-tree-instance.h"
#include "geon-tree-reference.h"
#include "geon-view.h"
#include "geon-viewport.h"
#include "geon-window.h"
#include "geon-world.h"

#undef __GEON_INSIDE__

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
