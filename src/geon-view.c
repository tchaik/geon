/* This file is part of Geon: geon-view.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-View"

#include "config.h"

#include <glib/gi18n.h>
#include <glib-object.h>

#include "geon-debug.h"
#include "geon-viewport.h"

#include "geon-view.h"

#ifdef GEON_RENDERING_OPENGL
# include "geon-gl-viewport.h"
#endif
#ifdef GEON_RENDERING_VULKAN
# include "geon-vk-viewport.h"
#endif

typedef struct
{
  GtkOverlay   *overlay;
  GeonViewport *viewport;
} GeonViewPrivate;

struct _GeonView
{
  GtkBox parent_instance;
};

G_DEFINE_TYPE_WITH_PRIVATE (GeonView, geon_view,
                            GTK_TYPE_BOX)

static GType
geon_view_get_renderer_type (const gchar *renderer_name)
{
  if (renderer_name == NULL)
    {
#ifdef GEON_RENDERING_OPENGL
      return GEON_TYPE_GL_VIEWPORT;
#else
# ifdef GEON_RENDERING_VULKAN
      return GEON_TYPE_VK_VIEWPORT;
# endif
#endif
    }
  else
    {
#ifdef GEON_RENDERING_OPENGL
      if (g_ascii_strcasecmp (renderer_name, "gl") == 0 ||
          g_ascii_strcasecmp (renderer_name, "opengl") == 0)
        return GEON_TYPE_GL_VIEWPORT;
#endif
#ifdef GEON_RENDERING_VULKAN
      if (g_ascii_strcasecmp (renderer_name, "vulkan") == 0)
        return GEON_TYPE_VK_VIEWPORT;
#endif

      g_error ("Unrecognized renderer \"%s\".",
               renderer_name);
    }

  return G_TYPE_INVALID;
}

static void
geon_view_finalize (GObject *object)
{
  GeonView *self = GEON_VIEW (object);
  GeonViewPrivate *priv;

  g_return_if_fail (GEON_IS_VIEW (self));

  priv = geon_view_get_instance_private (self);

  G_OBJECT_CLASS (geon_view_parent_class)->finalize (object);
}

static void
geon_view_class_init (GeonViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = geon_view_finalize;

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/gnome/geon/gtk/view.ui");

  gtk_widget_class_bind_template_child_private (widget_class, GeonView,
                                                overlay);
}

static void
geon_view_init (GeonView *self)
{
  GeonViewPrivate *priv;
  const gchar *renderer_name = g_getenv ("GEON_RENDERER");
  GType renderer_type;

  priv = geon_view_get_instance_private (self);

  renderer_type = geon_view_get_renderer_type (renderer_name);

  g_assert (renderer_type != G_TYPE_INVALID);

  gtk_widget_init_template (GTK_WIDGET (self));

  priv->viewport = g_object_new (renderer_type, NULL);

  gtk_widget_set_hexpand (GTK_WIDGET (priv->viewport), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (priv->viewport), TRUE);

  gtk_overlay_set_child (priv->overlay,
                         GTK_WIDGET (priv->viewport));
}

GeonView *
geon_view_new ()
{
  return g_object_new (GEON_TYPE_VIEW, NULL);
}

/* ex:set ts=2 sw=2 et: */
