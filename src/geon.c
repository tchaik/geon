/* This file is part of Geon: geon.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon"

#include <stdlib.h>

#include "geon-application.h"
#include "geon-debug.h"
#include "geon-log.h"

gboolean
geon_verbose_cb (const gchar *option_name,
                 const gchar *value,
                 gpointer data,
                 GError **erro)
{
  geon_log_increase_verbosity ();

  return TRUE;
}

gint
main (gint   argc,
      gchar *argv[])
{
  GeonApplication *application;
  g_autoptr(GOptionContext) context = NULL;
  gint status;
  GOptionEntry entries[] = {
    { "verbose", 'v',
      G_OPTION_FLAG_NO_ARG,
      G_OPTION_ARG_CALLBACK,
      geon_verbose_cb },

    { NULL }
  };

  geon_log_init ();

  context = g_option_context_new (NULL);

  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_set_ignore_unknown_options (context, TRUE);
  g_option_context_set_help_enabled (context, FALSE);

  g_option_context_parse (context, &argc, &argv, NULL);

  application = geon_application_new ();

  status = g_application_run (G_APPLICATION (application), argc, argv);

  g_object_unref (application);

  return status;
}

/* ex:set ts=2 sw=2 et: */
