/* This file is part of Geon: geon-application-private.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <libpeas/peas.h>

#include "geon-application.h"
#include "geon-file-analyzer.h"
#include "geon-file-formats.h"
#include "geon-file-loader.h"

G_BEGIN_DECLS

GMenuModel         *geon_application_get_hamburger_menu             (GeonApplication *self);

const gchar        *geon_application_get_analyzer_name_for_format   (GeonFileFormat *format);

GeonFileAnalyzer   *geon_application_get_analyzer_for_format        (GeonFileFormat *format);

const gchar        *geon_application_get_loader_name_for_format     (GeonFileFormat *format);

PeasPluginInfo     *geon_application_get_loader_info_for_format     (GeonFileFormat *format);

GeonFileLoader     *geon_application_new_loader_for_format          (GeonFileFormat *format);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
