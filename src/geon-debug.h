/* This file is part of Geon: geon-debug.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

G_BEGIN_DECLS

#ifndef G_LOG_LEVEL_TRACE
# define G_LOG_LEVEL_TRACE ((GLogLevelFlags)(1 << G_LOG_LEVEL_USER_SHIFT))
#endif

#ifdef GEON_ENABLE_TRACE
# define GEON_ENTER                                                          \
   g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE, "ENTER: %s():%d",                  \
         G_STRFUNC, __LINE__)
# define GEON_RETURN                                                         \
   G_STMT_START {                                                            \
      g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE, "EXIT: %s():%d",                \
            G_STRFUNC, __LINE__);                                            \
      return;                                                                \
   } G_STMT_END
# define GEON_RETURN_VAL(val)                                                \
    G_STMT_START {                                                           \
      g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE, "EXIT: %s():%d ",               \
            G_STRFUNC, __LINE__);                                            \
      return val;                                                            \
   } G_STMT_END
#else
# define GEON_ENTER           G_STMT_START {             } G_STMT_END
# define GEON_RETURN          G_STMT_START { return;     } G_STMT_END
# define GEON_RETURN_VAL(val) G_STMT_START { return val; } G_STMT_END
#endif

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
