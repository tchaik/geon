/* This file is part of Geon: geon-window.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-Window"

#include "config.h"

#include <glib/gi18n.h>
#include <glib-object.h>

#include "geon-application.h"
#include "geon-application-private.h"
#include "geon-debug.h"
#include "geon-view.h"

#include "geon-window.h"

typedef enum
{
  GEON_WINDOW_STATE_INITIALIZING,
  GEON_WINDOW_STATE_WAITING,
  GEON_WINDOW_STATE_LOADING,
  GEON_WINDOW_STATE_RUNNING,
} GeonWindowSate;

typedef struct
{
  GeonWindowSate  state;

  GeonWorld      *world;

  GtkHeaderBar   *header_bar;
  GtkMenuButton  *hamburger_button;

  GtkBox         *main_box;
  GtkStack       *main_stack;

  GtkBox         *initial_page_box;
  GtkButton      *open_button;

  GtkBox         *loading_page_box;
  GCancellable   *loading_cancellable;
  GtkProgressBar *progress_bar;
  guint           progress_pulse_source_id;
  GtkButton      *cancel_button;

  GtkBox         *viewer_page_box;
  GeonView       *main_view;
} GeonWindowPrivate;

struct _GeonWindow
{
  GtkApplicationWindow parent_instance;
};

enum {
  PROP_0,
  PROP_WORLD,
  N_PROPS
};

G_DEFINE_TYPE_WITH_PRIVATE (GeonWindow, geon_window,
                            GTK_TYPE_APPLICATION_WINDOW)

static GParamSpec *properties[N_PROPS];

static gboolean
geon_window_pulse_progress_bar_cb (gpointer user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;

  g_assert (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  if (priv->state != GEON_WINDOW_STATE_LOADING)
    {
      priv->progress_pulse_source_id = 0;
      return G_SOURCE_REMOVE;
    }

  gtk_progress_bar_pulse (priv->progress_bar);

  return G_SOURCE_CONTINUE;
}

static void
geon_window_update_state (GeonWindow     *self,
                          GeonWindowSate  new_state)
{
  GeonWindowPrivate *priv;

  g_assert (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  switch (new_state)
    {
    case GEON_WINDOW_STATE_WAITING:
      gtk_stack_set_visible_child_name (priv->main_stack,
                                        "initial_page");
      break;

    case GEON_WINDOW_STATE_LOADING:
      if (priv->state == GEON_WINDOW_STATE_WAITING)
        {
          priv->progress_pulse_source_id =
            g_timeout_add (100, geon_window_pulse_progress_bar_cb, self);
          gtk_progress_bar_set_text (priv->progress_bar,
                                     _("Analysing file…"));
        }
      else if (priv->state == GEON_WINDOW_STATE_LOADING)
        {
          if (priv->progress_pulse_source_id)
            {
              g_source_remove (priv->progress_pulse_source_id);
              priv->progress_pulse_source_id = 0;
            }

          gtk_progress_bar_set_fraction (priv->progress_bar, 0.0);

          if (priv->world == NULL)
            {
              gtk_progress_bar_set_text (priv->progress_bar,
                                         _("Failed"));
            }
          else
            {
              gtk_progress_bar_set_text (priv->progress_bar,
                                         _("Loading file…"));
            }
        }

      gtk_stack_set_visible_child_name (priv->main_stack,
                                        "loading_page");
      break;

    case GEON_WINDOW_STATE_RUNNING:
      gtk_stack_set_visible_child_name (priv->main_stack,
                                        "viewer_page");
      break;

    default:
      g_assert_not_reached ();
    }

  priv->state = new_state;
}

static void
geon_window_load_world_geometry_cb (GObject      *source_object,
                                    GAsyncResult *result,
                                    gpointer      user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;
  GTask *task = G_TASK (result);
  gboolean status;
  g_autoptr(GError) error = NULL;
  GtkDialogFlags error_dialog_flags =
    GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;
  GtkWidget *error_dialog;

  g_assert (GEON_IS_WINDOW (self));
  g_assert (G_IS_TASK (task));

  priv = geon_window_get_instance_private (self);

  status = geon_world_load_model_geometry_finish (priv->world, result, &error);

  g_object_unref (task);

  if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
      geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
  else if (status == FALSE)
    {
      error_dialog = gtk_message_dialog_new (GTK_WINDOW (self),
                                             error_dialog_flags,
                                             GTK_MESSAGE_ERROR,
                                             GTK_BUTTONS_CLOSE,
                                             "Error loading geometry");
      gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (error_dialog),
                                                "%s", error->message);

      g_signal_connect (error_dialog,
                        "response",
                        G_CALLBACK (gtk_window_destroy),
                        NULL);

      gtk_widget_show (error_dialog);

      g_set_object (&priv->world, NULL);

      geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
    }
  else
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_WORLD]);
}

static void
geon_window_load_world_tree_cb (GObject      *source_object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;
  GTask *task = G_TASK (result);
  gboolean status;
  g_autoptr(GError) error = NULL;
  GtkDialogFlags error_dialog_flags =
    GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;
  GtkWidget *error_dialog;

  g_assert (GEON_IS_WINDOW (self));
  g_assert (G_IS_TASK (task));

  priv = geon_window_get_instance_private (self);

  status = geon_world_load_model_tree_finish (priv->world, result, &error);

  g_clear_object (&priv->loading_cancellable);
  g_object_unref (task);

  if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
    geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
  else if (status == FALSE)
    {
      error_dialog = gtk_message_dialog_new (GTK_WINDOW (self),
                                             error_dialog_flags,
                                             GTK_MESSAGE_ERROR,
                                             GTK_BUTTONS_CLOSE,
                                             "Failed loading assembly tree");
      gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (error_dialog),
                                                "%s", error->message);

      g_signal_connect (error_dialog,
                        "response",
                        G_CALLBACK (gtk_window_destroy),
                        NULL);

      gtk_widget_show (error_dialog);

      g_set_object (&priv->world, NULL);

      geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
    }
  else
    {
      geon_window_update_state (self, GEON_WINDOW_STATE_RUNNING);

      if (geon_world_is_assembly (priv->world))
        g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_WORLD]);
      else
        {
          priv->loading_cancellable = g_cancellable_new ();

          geon_world_load_model_geometry_async (priv->world,
                                                priv->loading_cancellable,
                                                geon_window_load_world_geometry_cb,
                                                self);
        }
    }
}

static void
geon_window_create_new_world_cb (GObject      *source_object,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;
  GTask *task = G_TASK (result);
  GeonWorld *world;
  g_autoptr(GError) error = NULL;
  GtkDialogFlags error_dialog_flags =
    GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;
  GtkWidget *error_dialog;

  g_assert (GEON_IS_WINDOW (self));
  g_assert (G_IS_TASK (task));

  priv = geon_window_get_instance_private (self);

  world = geon_world_new_for_file_finish (result, &error);

  g_clear_object (&priv->loading_cancellable);
  g_object_unref (task);

  if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
    geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
  else if (!GEON_IS_WORLD (world))
    {
      geon_window_update_state (self, GEON_WINDOW_STATE_LOADING);

      error_dialog = gtk_message_dialog_new (GTK_WINDOW (self),
                                             error_dialog_flags,
                                             GTK_MESSAGE_ERROR,
                                             GTK_BUTTONS_CLOSE,
                                             "Error loading file");
      gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (error_dialog),
                                                "%s", error->message);

      g_signal_connect (error_dialog,
                        "response",
                        G_CALLBACK (gtk_window_destroy),
                        NULL);

      gtk_widget_show (error_dialog);

      g_set_object (&priv->world, NULL);

      geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
    }
  else
    {
      if (g_set_object (&priv->world, world))
        {
          priv->loading_cancellable = g_cancellable_new ();

          geon_world_load_model_tree_async (priv->world,
                                            priv->loading_cancellable,
                                            geon_window_load_world_tree_cb,
                                            self);
      }
    }
}

static void
geon_window_create_new_world (GeonWindow  *self,
                              GFile       *file,
                              const gchar *uri)
{
  GeonWindowPrivate *priv;
  g_autoptr(GFile) target_file = NULL;

  GEON_ENTER;

  g_assert (GEON_IS_WINDOW (self));
  g_assert (G_IS_FILE (file) || uri != NULL);

  priv = geon_window_get_instance_private (self);

  if (file == NULL)
    target_file = g_file_new_for_uri (uri);
  else
    target_file = g_object_ref (file);

  priv->loading_cancellable = g_cancellable_new ();

  geon_world_new_for_file_async (target_file,
                                 priv->loading_cancellable,
                                 geon_window_create_new_world_cb,
                                 self);

  geon_window_update_state (self, GEON_WINDOW_STATE_LOADING);

  GEON_RETURN;
}

static void
geon_window_do_open_uri (GeonWindow  *self,
                         const gchar *file_uri)
{
  GeonWindowPrivate *priv;
  GtkApplication *application = NULL;

  g_assert (GEON_IS_WINDOW (self));
  g_assert (file_uri != NULL);

  priv = geon_window_get_instance_private (self);

  if (geon_window_is_empty (self))
    geon_window_create_new_world (self, NULL, file_uri);
  else
    {
      application = gtk_window_get_application (GTK_WINDOW (self));
      if (!GEON_IS_APPLICATION (application))
        application = GTK_APPLICATION (g_application_get_default ());

      g_assert (GEON_IS_APPLICATION (application));

      geon_application_open_uri (GEON_APPLICATION (application), file_uri);
    }
}

static void
geon_window_open_file_chooser_response_cb (GtkNativeDialog *dialog,
                                           gint             response_id,
                                           gpointer         user_data)
{
  GtkFileChooser *file_chooser = GTK_FILE_CHOOSER (dialog);
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;
  g_autofree const gchar *file_uri = NULL;
  g_autoptr(GFile) file = NULL;

  g_assert (GTK_IS_FILE_CHOOSER (file_chooser));
  g_assert (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  if (response_id == GTK_RESPONSE_ACCEPT)
    {
      file = gtk_file_chooser_get_file (file_chooser);
      file_uri = g_file_get_uri (file);

      geon_window_do_open_uri (self, file_uri);
    }

  gtk_native_dialog_destroy (dialog);

  g_object_unref (dialog);
}

static void
geon_window_open_file_activated_cb (GSimpleAction *action,
                                    GVariant      *parameter,
                                    gpointer       user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;
  g_autofree const gchar *file_uri = NULL;
  GtkFileChooserNative *file_chooser;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (parameter != NULL);
  g_assert (g_variant_is_of_type (parameter, G_VARIANT_TYPE_STRING));
  g_assert (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  file_uri = g_variant_dup_string (parameter, NULL);

  if (g_strcmp0 (file_uri, "file-chooser") == 0)
    {
      file_chooser = gtk_file_chooser_native_new (_("Open File"),
                                                  GTK_WINDOW (self),
                                                  GTK_FILE_CHOOSER_ACTION_OPEN,
                                                  _("_Open"),
                                                  _("_Cancel"));

      g_signal_connect (file_chooser,
                        "response",
                        G_CALLBACK (geon_window_open_file_chooser_response_cb),
                        self);

      gtk_native_dialog_show (GTK_NATIVE_DIALOG (file_chooser));
    }
  else if (file_uri == NULL)
    {
      g_error ("Open-file action activated with invalid parameter!");
      return;
    }
  else
    geon_window_do_open_uri (self, file_uri);
}

static void
geon_window_cancel_loading_activated_cb (GSimpleAction *action,
                                         GVariant      *parameter,
                                         gpointer       user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  if (priv->loading_cancellable != NULL)
    g_cancellable_cancel (priv->loading_cancellable);
}

static void
geon_window_close_activated_cb (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;
  gboolean keep_empty;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (parameter != NULL);
  g_assert (g_variant_is_of_type (parameter, G_VARIANT_TYPE_BOOLEAN));
  g_assert (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  keep_empty = g_variant_get_boolean (parameter);

  g_clear_object (&priv->world);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_WORLD]);

  if (keep_empty == TRUE)
    geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
  else
    gtk_window_close (GTK_WINDOW (self));
}

static gboolean
geon_window_file_drop_cb (GtkDropTarget *target,
                          const GValue  *value,
                          gdouble        x,
                          gdouble        y,
                          gpointer       user_data)
{
  GeonWindow *self = GEON_WINDOW (user_data);
  GeonWindowPrivate *priv;
  GVariant *parameter;
  GFile *file;
  g_autofree gchar *file_uri = NULL;

  g_assert (GEON_IS_WINDOW (self));
  g_assert (G_VALUE_HOLDS (value, G_TYPE_FILE));

  priv = geon_window_get_instance_private (self);

  file = g_value_get_object (value);
  file_uri = g_file_get_uri (file);

  parameter = g_variant_new_string (file_uri);

  g_action_group_activate_action (G_ACTION_GROUP (self),
                                  "open-file",
                                  g_variant_ref_sink (parameter));

  return TRUE;
}

static void
geon_window_constructed (GObject *object)
{
  GeonWindow *self = GEON_WINDOW (object);
  GeonWindowPrivate *priv;
  GtkApplication *application;
  GMenuModel *hamburger_menu;

  g_return_if_fail (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  G_OBJECT_CLASS (geon_window_parent_class)->constructed (object);

  application = gtk_window_get_application (GTK_WINDOW (self));
  if (!GEON_IS_APPLICATION (application))
    application = GTK_APPLICATION (g_application_get_default ());

  g_assert (GEON_IS_APPLICATION (application));

  hamburger_menu = geon_application_get_hamburger_menu (GEON_APPLICATION (application));

  gtk_menu_button_set_menu_model (priv->hamburger_button, hamburger_menu);

  geon_window_update_state (self, GEON_WINDOW_STATE_WAITING);
}

static void
geon_window_dispose (GObject *object)
{
  GeonWindow *self = GEON_WINDOW (object);
  GeonWindowPrivate *priv;

  g_return_if_fail (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  if (priv->progress_pulse_source_id)
    {
      g_source_remove (priv->progress_pulse_source_id);
      priv->progress_pulse_source_id = 0;
    }

  G_OBJECT_CLASS (geon_window_parent_class)->dispose (object);
}

static void
geon_window_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  GeonWindow *self = GEON_WINDOW (object);

  g_return_if_fail (GEON_IS_WINDOW (self));

  switch (property_id)
    {
    case PROP_WORLD:
      g_value_set_object (value, geon_window_get_world (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
geon_window_finalize (GObject *object)
{
  GeonWindow *self = GEON_WINDOW (object);
  GeonWindowPrivate *priv;

  g_return_if_fail (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  g_clear_object (&priv->world);

  G_OBJECT_CLASS (geon_window_parent_class)->finalize (object);
}

static void
geon_window_class_init (GeonWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->constructed = geon_window_constructed;
  object_class->dispose = geon_window_dispose;
  object_class->get_property = geon_window_get_property;
  object_class->finalize = geon_window_finalize;

  properties[PROP_WORLD] =
    g_param_spec_object ("world",
                         "World",
                         "The 3D world loaded into that window",
                         GEON_TYPE_WORLD,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/gnome/geon/gtk/window.ui");

  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                header_bar);
  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                hamburger_button);

  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                main_box);
  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                main_stack);

  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                initial_page_box);
  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                open_button);

  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                loading_page_box);
  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                progress_bar);
  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                cancel_button);

  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                viewer_page_box);
  gtk_widget_class_bind_template_child_private (widget_class, GeonWindow,
                                                main_view);
}

static GActionEntry action_entries[] = {
  { "cancel-loading", geon_window_cancel_loading_activated_cb, NULL, NULL, NULL },
  { "close", geon_window_close_activated_cb, "b", NULL, NULL },
  { "open-file", geon_window_open_file_activated_cb, "s", NULL, NULL }
};

static void
geon_window_init (GeonWindow *self)
{
  GeonWindowPrivate *priv;
  GtkDropTarget *drop_target;

  priv = geon_window_get_instance_private (self);

  priv->state = GEON_WINDOW_STATE_INITIALIZING;

  gtk_widget_init_template (GTK_WIDGET (self));

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   action_entries,
                                   G_N_ELEMENTS (action_entries),
                                   self);

  drop_target = gtk_drop_target_new (G_TYPE_FILE, GDK_ACTION_COPY);

  gtk_widget_add_controller (GTK_WIDGET (priv->initial_page_box),
                             GTK_EVENT_CONTROLLER (drop_target));

  g_signal_connect (drop_target,
                    "drop",
                    G_CALLBACK (geon_window_file_drop_cb),
                    self);
}

GeonWindow *
geon_window_new (GtkApplication *application)
{
  g_return_val_if_fail (GTK_IS_APPLICATION (application), NULL);

  return g_object_new (GEON_TYPE_WINDOW,
                       "application", application,
                       NULL);
}

gboolean
geon_window_is_empty (GeonWindow *self)
{
  GeonWindowPrivate *priv;

  g_return_val_if_fail (GEON_IS_WINDOW (self), FALSE);

  priv = geon_window_get_instance_private (self);

  return priv->world == NULL;
}

void
geon_window_open_uri (GeonWindow  *self,
                      const gchar *file_uri)
{
  GeonWindowPrivate *priv;
  GVariant *parameter;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  if (file_uri == NULL)
    parameter = g_variant_new_string ("file-chooser");
  else
    parameter = g_variant_new_string (file_uri);

  g_action_group_activate_action (G_ACTION_GROUP (self),
                                  "open-file",
                                  parameter);

  GEON_RETURN;
}

void
geon_window_open_file (GeonWindow *self,
                       GFile      *file)
{
  GeonWindowPrivate *priv;
  GVariant *parameter;
  gchar *file_uri;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  if (file == NULL || !G_IS_FILE (file))
    parameter = g_variant_new_string ("file-chooser");
  else
    {
      file_uri = g_file_get_uri (file);
      parameter = g_variant_new_take_string (file_uri);
    }

  g_action_group_activate_action (G_ACTION_GROUP (self),
                                  "open-file",
                                  parameter);

  GEON_RETURN;
}

void
geon_window_close (GeonWindow *self,
                   gboolean    keep_empty)
{
  GeonWindowPrivate *priv;
  GVariant *parameter;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_WINDOW (self));

  priv = geon_window_get_instance_private (self);

  parameter = g_variant_new_boolean (keep_empty);

  g_action_group_activate_action (G_ACTION_GROUP (self),
                                  "close",
                                  parameter);

  GEON_RETURN;
}

GeonWorld *
geon_window_get_world (GeonWindow *self)
{
  GeonWindowPrivate *priv;

  g_return_val_if_fail (GEON_IS_WINDOW (self), NULL);

  priv = geon_window_get_instance_private (self);

  return priv->world;
}

GFile *
geon_window_get_opened_file (GeonWindow *self)
{
  GeonWindowPrivate *priv;

  g_return_val_if_fail (GEON_IS_WINDOW (self), NULL);

  priv = geon_window_get_instance_private (self);

  if (priv->world == NULL)
    return NULL;

  return geon_world_get_file (priv->world);
}

/* ex:set ts=2 sw=2 et: */
