/* This file is part of Geon: geon-application.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-Application"

#include "config.h"

#include <glib/gi18n.h>
#include <glib-object.h>
#include <locale.h>
#include <stdlib.h>

#include "geon-debug.h"
#include "geon-file-formats.h"

#include "geon-window.h"
#include "geon-world.h"

#ifdef GEON_RENDERING_OPENGL
# include "geon-gl-utils.h"
#endif
#ifdef GEON_RENDERING_VULKAN
# include "geon-vk-utils.h"
#endif

#include "geon-application.h"
#include "geon-application-private.h"

typedef struct
{
  PeasEngine       *analyzers_engine;
  PeasExtensionSet *analyzers_set;
  GHashTable       *analyzers_cache;

  PeasEngine       *loaders_engine;
  GHashTable       *loaders_cache;

  GHashTable       *loaded_worlds;
} GeonApplicationPrivate;

struct _GeonApplication
{
  AdwApplication parent_instance;
};

G_DEFINE_TYPE_WITH_PRIVATE (GeonApplication, geon_application,
                            ADW_TYPE_APPLICATION)

static GMenuModel *
geon_application_get_menu (GeonApplication *self,
                           const gchar     *id)
{
  GMenu *menu;

  g_assert (GEON_IS_APPLICATION (self));
  g_assert (id != NULL);

  menu = gtk_application_get_menu_by_id (GTK_APPLICATION (self), id);

  if (!G_IS_MENU_MODEL (menu))
    return NULL;

  return G_MENU_MODEL (g_object_ref_sink (menu));
}

static GeonWindow *
geon_application_get_empty_window (GeonApplication *self)
{
  GeonApplicationPrivate *priv;
  GeonWindow *empty_window = NULL;
  GList *windows;

  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  windows = gtk_application_get_windows (GTK_APPLICATION (self));

  while (windows != NULL)
    {
      GeonWindow *window = GEON_WINDOW (windows->data);

      if (geon_window_is_empty (window))
        {
          empty_window = window;
          break;
        }

      windows = windows->next;
    }

  if (empty_window == NULL)
    empty_window = geon_window_new (GTK_APPLICATION (self));

  return empty_window;
}

static GeonWindow *
geon_application_get_active_window (GeonApplication *self)
{
  GeonApplicationPrivate *priv;
  GtkWindow *window;

  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  g_assert (GEON_IS_WINDOW (window));

  return GEON_WINDOW (window);
}

static GeonWindow *
geon_application_get_window_by_file (GeonApplication *self,
                                     GFile           *file)
{
  GeonApplicationPrivate *priv;
  GList *windows;

  g_assert (GEON_IS_APPLICATION (self));
  g_assert (G_IS_FILE (file));

  priv = geon_application_get_instance_private (self);

  windows = gtk_application_get_windows (GTK_APPLICATION (self));

  while (windows != NULL)
    {
      GeonWindow *window = GEON_WINDOW (windows->data);
      GFile *opened_file;

      opened_file = geon_window_get_opened_file (window);

      if (g_file_equal (opened_file, file))
        return window;

      windows = windows->next;
    }

  return NULL;
}

static void
geon_application_about_activated_cb (GSimpleAction *action,
                                     GVariant      *parameter,
                                     gpointer       user_data)
{
  GeonApplication *self = GEON_APPLICATION (user_data);
  GeonApplicationPrivate *priv;
  GeonWindow *active_window;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  active_window = geon_application_get_active_window (self);

  gtk_show_about_dialog (GTK_WINDOW (active_window),
                         "comments", _("A 3D model viewer for GNOME"),
                         "copyright", "© 2018 Martin Blanchard.",
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "logo-icon-name", GEON_APPLICATION_ID,
                         "program-name", _("Geon"),
                         "version", PACKAGE_VERSION,
                         NULL);
}

static void
geon_application_close_activated_cb (GSimpleAction *action,
                                     GVariant      *parameter,
                                     gpointer       user_data)
{
  GeonApplication *self = GEON_APPLICATION (user_data);
  GeonApplicationPrivate *priv;
  GeonWindow *active_window;
  GList *windows;
  guint window_count;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  windows = gtk_application_get_windows (GTK_APPLICATION (self));
  window_count = g_list_length (windows);

  active_window = geon_application_get_active_window (self);

  if (window_count > 1)
    geon_window_close (active_window, FALSE);
  else if (window_count == 1)
    geon_window_close (active_window, TRUE);
}

static void
geon_application_close_all_activated_cb (GSimpleAction *action,
                                     GVariant      *parameter,
                                     gpointer       user_data)
{
  GeonApplication *self = GEON_APPLICATION (user_data);
  GeonApplicationPrivate *priv;
  GList *windows;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  windows = gtk_application_get_windows (GTK_APPLICATION (self));

  while (windows != NULL)
    {
      GeonWindow *window = GEON_WINDOW (windows->data);

      if (windows->prev != NULL)
        geon_window_close (window, FALSE);
      else
        geon_window_close (window, TRUE);

      windows = windows->next;
    }
}

static void
geon_application_open_file_activated_cb (GSimpleAction *action,
                                         GVariant      *parameter,
                                         gpointer       user_data)
{
  GeonApplication *self = GEON_APPLICATION (user_data);
  GeonApplicationPrivate *priv;
  g_autofree const gchar *file_uri = NULL;
  g_autoptr(GFile) file = NULL;
  GeonWindow *window;

  g_assert (G_IS_SIMPLE_ACTION (action));
  g_assert (parameter != NULL);
  g_assert (g_variant_is_of_type (parameter, G_VARIANT_TYPE_STRING));
  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  file_uri = g_variant_dup_string (parameter, NULL);

  if (file_uri == NULL || !g_strcmp0 (file_uri, "file-chooser"))
    {
      window = geon_application_get_active_window (self);
      geon_window_open_file (window, NULL);
    }
  else
    {
      file = g_file_new_for_uri (file_uri);
      window = geon_application_get_window_by_file (self, file);

      if (window == NULL)
        {
          window = geon_application_get_empty_window (self);
          geon_window_open_file (window, file);
        }

        gtk_window_present (GTK_WINDOW (window));
    }
}

static void
geon_application_open (GApplication  *application,
                       GFile        **files,
                       gint           n_files,
                       const gchar   *hint)
{
  GeonApplication *self = GEON_APPLICATION (application);
  GeonApplicationPrivate *priv;
  gint i;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_APPLICATION (self));
  g_return_if_fail (files != NULL);
  g_return_if_fail (n_files > 0);

  priv = geon_application_get_instance_private (self);

  for (i = 0; i < n_files; i++)
    {
      GFile *file = files[i];
      GVariant *parameter;
      gchar *file_uri;

      g_assert (G_IS_FILE (file));

      file_uri = g_file_get_uri (file);
      parameter = g_variant_new_take_string (file_uri);

      g_action_group_activate_action (G_ACTION_GROUP (self),
                                      "open-file",
                                      parameter);
    }

  GEON_RETURN;
}

static void
geon_application_activate (GApplication *application)
{
  GeonApplication *self = GEON_APPLICATION (application);
  GeonApplicationPrivate *priv;
  GeonWindow *window;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  window = geon_application_get_empty_window (self);
  gtk_window_present (GTK_WINDOW (window));

  GEON_RETURN;
}

static const GList *
geon_application_list_plugins (PeasEngine  *engine,
                               const gchar *folder_name)
{
  g_autofree gchar *resource_path = NULL;
  g_autofree gchar *system_path = NULL;
  g_autofree gchar *user_path = NULL;

  g_assert (PEAS_IS_ENGINE (engine));
  g_assert (folder_name != NULL);

  user_path = g_build_filename (g_get_user_data_dir (),
                                PACKAGE_STRING,
                                folder_name,
                                NULL);

  peas_engine_add_search_path (engine, user_path, NULL);

  system_path = g_build_filename (PACKAGE_LIBDIR,
                                  PACKAGE_STRING,
                                  folder_name,
                                  NULL);

  peas_engine_add_search_path (engine, system_path, NULL);

  resource_path = g_strdup_printf ("resource:///org/gnome/geon/plugins/%s",
                                   folder_name);

  peas_engine_add_search_path (engine, resource_path, NULL);

  return peas_engine_get_plugin_list (engine);
}

static void
geon_application_analyzer_added_cb (PeasExtensionSet *extension_set,
                                    PeasPluginInfo   *plugin_info,
                                    PeasExtension    *extension,
                                    gpointer          user_data)
{
  GeonApplication *self = GEON_APPLICATION (user_data);
  GeonApplicationPrivate *priv;
  GeonFileAnalyzer *analyzer;
  g_autofree gchar **formats_array = NULL;
  const gchar *formats_list;
  guint i;

  g_assert (PEAS_IS_EXTENSION_SET (extension_set));
  g_assert (plugin_info != NULL);
  g_assert (GEON_IS_FILE_ANALYZER (extension));
  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  g_assert (priv->analyzers_cache != NULL);

  g_debug ("Discovered file analyzer '%s'",
           peas_plugin_info_get_module_name (plugin_info));

  analyzer = GEON_FILE_ANALYZER (extension);

  geon_file_analyzer_register_file_formats (analyzer);

  formats_list = peas_plugin_info_get_external_data (plugin_info,
                                                     "X-SupportedFileFormats");
  formats_array = g_strsplit (formats_list, ",", -1);

  for (i = 0; formats_array[i] != NULL; i++)
    {
      gchar *format_name = formats_array[i];

      if (geon_file_formats_get_by_name (format_name) == NULL)
        {
          g_warning ("'%s' format isn't registered", format_name);
          continue;
        }
      else if (g_hash_table_contains (priv->analyzers_cache, format_name))
        {
          g_warning ("'%s' analyzer already registered", format_name);
          continue;
        }

      g_debug ("Registered analyzer for format '%s'", format_name);

      g_hash_table_insert (priv->analyzers_cache,
                           format_name,
                           analyzer);
    }
}

static void
geon_application_discover_and_load_analyzers (GeonApplication *self)
{
  GeonApplicationPrivate *priv;
  PeasExtensionSet *extension_set;
  const GList *plugins_list;
  GList *iter;

  GEON_ENTER;

  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  extension_set = peas_extension_set_new (priv->analyzers_engine,
                                          GEON_TYPE_FILE_ANALYZER,
                                          NULL);
  g_signal_connect (extension_set,
                    "extension-added",
                    G_CALLBACK (geon_application_analyzer_added_cb),
                    self);

  plugins_list = geon_application_list_plugins (priv->analyzers_engine,
                                                "analyzers");

  for (iter = (GList *)plugins_list; iter != NULL; iter = iter->next)
    {
      PeasPluginInfo *plugin_info = iter->data;
      gboolean status;

      status = peas_engine_load_plugin (priv->analyzers_engine,
                                        plugin_info);
      if (status == FALSE)
        {
          g_warning ("Failed loading '%s' analyzer module",
                     peas_plugin_info_get_module_name (plugin_info));
          continue;
        }
    }

  GEON_RETURN;
}

static void
geon_application_foreach_loader_cb (gpointer data,
                                    gpointer user_data)
{
  GeonApplication *self = GEON_APPLICATION (user_data);
  PeasPluginInfo *plugin_info = (PeasPluginInfo *)data;
  GeonApplicationPrivate *priv;
  g_autofree gchar **formats_array = NULL;
  const gchar *module_name, *formats_list;
  guint i;

  g_assert (GEON_IS_APPLICATION (self));
  g_assert (plugin_info != NULL);

  priv = geon_application_get_instance_private (self);

  g_assert (priv->loaders_cache != NULL);

  module_name = peas_plugin_info_get_module_name (plugin_info);

  g_debug ("Discovered file loader '%s'", module_name);

  formats_list = peas_plugin_info_get_external_data (plugin_info,
                                                     "X-SupportedFileFormats");
  formats_array = g_strsplit (formats_list, ",", -1);

  for (i = 0; formats_array[i] != NULL; i++)
    {
      gchar *format_name = formats_array[i];

      if (geon_file_formats_get_by_name (format_name) == NULL)
        {
          g_warning ("'%s' format isn't registered", format_name);
          continue;
        }
      else if (g_hash_table_contains (priv->loaders_cache, format_name))
        {
          g_warning ("'%s' loader already registered", format_name);
          continue;
        }

      g_debug ("Registered loader for format '%s'", format_name);

      g_hash_table_insert (priv->loaders_cache,
                           format_name,
                           g_strdup (module_name));
    }
}

static void
geon_application_discover_loaders (GeonApplication *self)
{
  GeonApplicationPrivate *priv;
  const GList *plugins_list;

  GEON_ENTER;

  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  plugins_list = geon_application_list_plugins (priv->loaders_engine,
                                                "loaders");

  g_list_foreach ((GList *)plugins_list,
                  geon_application_foreach_loader_cb,
                  self);

  GEON_RETURN;
}

static void
geon_application_discover_modules (GeonApplication *self)
{
  GeonApplicationPrivate *priv;

  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  priv->analyzers_engine = peas_engine_new ();

  priv->analyzers_cache = g_hash_table_new_full (g_str_hash, g_str_equal,
                                                 g_free, NULL);
  geon_application_discover_and_load_analyzers (self);

  priv->loaders_engine = peas_engine_new ();

  priv->loaders_cache = g_hash_table_new_full (g_str_hash, g_str_equal,
                                               g_free, g_free);
  geon_application_discover_loaders (self);
}

typedef struct {
  const gchar     *action;
  guint            key;
  GdkModifierType  modifiers;
} GeonAccelEntry;

static GeonAccelEntry accel_entries[] = {
  { "app.close", GDK_KEY_W, GDK_CONTROL_MASK },
  { "app.close-all", GDK_KEY_W, GDK_CONTROL_MASK | GDK_SHIFT_MASK },
  { "app.open-file::file-chooser", GDK_KEY_O, GDK_CONTROL_MASK },
};

static void
geon_application_load_default_accelerators (GeonApplication      *self,
                                            const GeonAccelEntry *entries,
                                            gint                  n_entries)
{
  GeonApplicationPrivate *priv;
  guint i;

  g_assert (GEON_IS_APPLICATION (self));
  g_assert (entries != NULL || n_entries == 0);

  priv = geon_application_get_instance_private (self);

  for (i = 0; n_entries == -1 ? entries[i].action != NULL : i < n_entries; i++)
    {
      const GeonAccelEntry *entry = &entries[i];

      g_assert (entry->action != NULL);
      g_assert (entry->key != 0);
      g_assert (entry->modifiers != 0);

      geon_application_set_accelerator (self,
                                        entry->action,
                                        entry->key,
                                        entry->modifiers);
    }
}

static void
geon_application_load_accelerators (GeonApplication *self)
{
  GeonApplicationPrivate *priv;
  g_autoptr (GFile) map_file = NULL;
  g_autofree gchar *map_path = NULL;

  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  geon_application_load_default_accelerators (self,
                                              accel_entries,
                                              G_N_ELEMENTS (accel_entries));
}

static void
geon_application_startup (GApplication *application)
{
  GeonApplication *self = GEON_APPLICATION (application);
  GeonApplicationPrivate *priv;
  AdwStyleManager *style_manager;

  GEON_ENTER;

  g_return_if_fail (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  G_APPLICATION_CLASS (geon_application_parent_class)->startup (application);

  style_manager = adw_style_manager_get_default ();

  adw_style_manager_set_color_scheme(style_manager,
                                     ADW_COLOR_SCHEME_PREFER_DARK);

  geon_application_load_accelerators (self);

  GEON_RETURN;
}

void
geon_application_file_format_cb (GeonFileFormat *format,
                                 gpointer        user_data)
{
  GList **formats = user_data;

  g_assert (formats != NULL);

  *formats = g_list_append (*formats, format);
}

void
geon_application_print_info (GeonApplication *self)
{
  GeonApplicationPrivate *priv;
  GList *i, *formats = NULL;

  g_assert (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  geon_file_formats_foreach (geon_application_file_format_cb,
                             &formats);

  if (formats == NULL)
    g_print ("* File formats: (none)\n");
  else
    {
      formats = g_list_sort (formats,
                             (GCompareFunc)geon_file_format_compare);

      g_print ("* File formats:\n");
    }


  for (i = formats; i != NULL; i = i->next)
    {
      GeonFileFormat *format = i->data;
      PeasPluginInfo *plugin_info;
      const gchar *module_name, *module_dir;

      plugin_info = geon_application_get_loader_info_for_format (format);

      if (plugin_info == NULL)
        continue;

      module_name = peas_plugin_info_get_module_name (plugin_info);
      module_dir = peas_plugin_info_get_module_dir (plugin_info);

      g_print ("   %s: %s (%s)\n",
               geon_file_format_get_name (format),
               module_name,
               module_dir);
    }

  g_list_free (formats);
}

static gboolean
geon_application_local_command_line (GApplication   *application,
                                     gchar        ***arguments,
                                     gint           *exit_status)
{
  GeonApplication *self = GEON_APPLICATION (application);
  GeonApplicationPrivate *priv;
  GError *error = NULL;
  g_autoptr(GOptionContext) context = NULL;
  gboolean print_info = FALSE;
  gboolean list_renderers = FALSE;
  gboolean print_version = FALSE;
  gboolean status = FALSE;
  GOptionEntry entries[] = {
    { "info", 0,
      G_OPTION_FLAG_NONE,
      G_OPTION_ARG_NONE,
      &print_info,
      N_("Print miscellaneous information"),
      NULL },

    { "list-renderers", 0,
      G_OPTION_FLAG_NONE,
      G_OPTION_ARG_NONE,
      &list_renderers,
      N_("List available renderers"),
      NULL },

    { "verbose", 'v',
      G_OPTION_FLAG_NO_ARG | G_OPTION_FLAG_IN_MAIN,
      G_OPTION_ARG_CALLBACK,
      NULL,
      N_("Increase verbosity, may be specified multiple times"),
      NULL },

    { "version", 'V',
      G_OPTION_FLAG_NONE,
      G_OPTION_ARG_NONE,
      &print_version,
      N_("Show the application’s version"),
      NULL },

    { NULL }
  };

  GEON_ENTER;

  g_return_val_if_fail (GEON_IS_APPLICATION (self), FALSE);
  g_return_val_if_fail (arguments != NULL, FALSE);
  g_return_val_if_fail (exit_status != NULL, FALSE);

  priv = geon_application_get_instance_private (self);

  *exit_status = EXIT_SUCCESS;

  context = g_option_context_new (_("FILE"));

  g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);

  if (!g_option_context_parse_strv (context, arguments, &error))
    {
      g_printerr ("%s\n", error->message);
      *exit_status = EXIT_FAILURE;
      GEON_RETURN_VAL (TRUE);
    }

  if (print_version == TRUE)
    {
      g_print ("%s\n", PACKAGE_VERSION);
      GEON_RETURN_VAL (TRUE);
    }

  if (list_renderers == TRUE)
    {
#ifdef GEON_RENDERING_OPENGL
      g_print ("opengl\n");
#endif
#ifdef GEON_RENDERING_VULKAN
      g_print ("vulkan\n");
#endif
      GEON_RETURN_VAL (TRUE);
    }

  geon_application_discover_modules (self);

  if (print_info == TRUE)
    {
      geon_application_print_info (self);
#ifdef GEON_RENDERING_OPENGL
      geon_gl_utils_print_info ();
#endif
#ifdef GEON_RENDERING_VULKAN
      geon_vk_utils_print_info ();
#endif
      GEON_RETURN_VAL (TRUE);
    }

  status = G_APPLICATION_CLASS (geon_application_parent_class)
    ->local_command_line (application, arguments, exit_status);

  GEON_RETURN_VAL (status);
}

static void
geon_application_update_action_statuses (GeonApplication *self)
{
  GeonApplicationPrivate *priv;
  GAction *close_action, *close_all_action;

  g_return_if_fail (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  close_action = g_action_map_lookup_action (G_ACTION_MAP (self),
                                             "close");
  close_all_action = g_action_map_lookup_action (G_ACTION_MAP (self),
                                                 "close-all");

  if (g_hash_table_size (priv->loaded_worlds) > 0)
    {
      g_simple_action_set_enabled (G_SIMPLE_ACTION (close_action), TRUE);
      g_simple_action_set_enabled (G_SIMPLE_ACTION (close_all_action), TRUE);
    }
  else
    {
      g_simple_action_set_enabled (G_SIMPLE_ACTION (close_action), FALSE);
      g_simple_action_set_enabled (G_SIMPLE_ACTION (close_all_action), FALSE);
    }
}

static void
geon_application_window_world_changed (GObject    *object,
                                       GParamSpec *pspec,
                                       gpointer    user_data)
{
  GeonApplication *self = GEON_APPLICATION (user_data);
  GeonWindow *window = GEON_WINDOW (object);
  const gchar *pname = g_intern_string ("world");
  GeonApplicationPrivate *priv;
  GeonWorld *world;

  g_return_if_fail (GEON_IS_APPLICATION (self));
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (g_param_spec_get_name (pspec) == pname);
  g_return_if_fail (GEON_IS_WINDOW (window));

  priv = geon_application_get_instance_private (self);

  world = geon_window_get_world (window);

  if (GEON_IS_WORLD (world))
    g_hash_table_insert (priv->loaded_worlds, window, world);
  else
    g_hash_table_remove (priv->loaded_worlds, window);

  geon_application_update_action_statuses (self);
}

static void
geon_application_window_added_cb (GtkApplication *application,
                                  GtkWindow      *application_window,
                                  gpointer        user_data)
{
  GeonApplication *self = GEON_APPLICATION (application);
  GeonWindow *window = GEON_WINDOW (application_window);
  GeonApplicationPrivate *priv;

  g_return_if_fail (GEON_IS_APPLICATION (self));
  g_return_if_fail (GEON_IS_WINDOW (window));

  priv = geon_application_get_instance_private (self);

  g_signal_connect (GEON_WINDOW (window),
                    "notify::world",
                    G_CALLBACK (geon_application_window_world_changed),
                    self);

  geon_application_update_action_statuses (self);
}

static void
geon_application_window_removed_cb (GtkApplication *application,
                                    GtkWindow      *application_window,
                                    gpointer        user_data)
{
  GeonApplication *self = GEON_APPLICATION (application);
  GeonWindow *window = GEON_WINDOW (application_window);
  GeonApplicationPrivate *priv;

  g_return_if_fail (GEON_IS_APPLICATION (self));
  g_return_if_fail (GEON_IS_WINDOW (window));

  priv = geon_application_get_instance_private (self);

  g_hash_table_remove (priv->loaded_worlds, window);

  geon_application_update_action_statuses (self);
}

static void
geon_application_finalize (GObject *object)
{
  GeonApplication *self = GEON_APPLICATION (object);
  GeonApplicationPrivate *priv;

  g_return_if_fail (GEON_IS_APPLICATION (self));

  priv = geon_application_get_instance_private (self);

  g_clear_pointer (&priv->loaders_cache, g_hash_table_destroy);

  geon_file_formats_flush_out_registrations ();

  g_clear_pointer (&priv->analyzers_cache, g_hash_table_destroy);
  g_clear_object (&priv->analyzers_set);

  g_clear_pointer (&priv->loaded_worlds, g_hash_table_destroy);

  G_OBJECT_CLASS (geon_application_parent_class)->finalize (object);
}

static void
geon_application_class_init (GeonApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *g_application_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = geon_application_finalize;

  g_application_class->startup = geon_application_startup;
  g_application_class->local_command_line = geon_application_local_command_line;
  g_application_class->activate = geon_application_activate;
  g_application_class->open = geon_application_open;
}

static GActionEntry action_entries[] = {
  { "about", geon_application_about_activated_cb, NULL, NULL, NULL },
  { "close", geon_application_close_activated_cb, NULL, NULL, NULL },
  { "close-all", geon_application_close_all_activated_cb, NULL, NULL, NULL },
  { "open-file", geon_application_open_file_activated_cb, "s", NULL, NULL },
};

static void
geon_application_init (GeonApplication *self)
{
  GeonApplicationPrivate *priv;

  setlocale (LC_ALL, "");

  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  g_set_application_name (_("Geon"));

  gtk_window_set_default_icon_name (GEON_APPLICATION_ID);

  priv = geon_application_get_instance_private (self);

  priv->loaded_worlds = g_hash_table_new (NULL, NULL);

  g_signal_connect (self,
                    "window-added",
                    G_CALLBACK (geon_application_window_added_cb),
                    self);

  g_signal_connect (self,
                    "window-removed",
                    G_CALLBACK (geon_application_window_removed_cb),
                    self);

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   action_entries,
                                   G_N_ELEMENTS (action_entries),
                                   self);
}

GeonApplication *
geon_application_new (void)
{
  return g_object_new (GEON_TYPE_APPLICATION,
                       "application-id", GEON_APPLICATION_ID,
                       "resource-base-path", GEON_RESOURCE_BASE_PATH,
                       "flags", G_APPLICATION_HANDLES_OPEN,
                       NULL);
}

void
geon_application_open_uri (GeonApplication *self,
                           const gchar     *file_uri)
{
  GeonApplicationPrivate *priv;
  GVariant *parameter;

  g_return_if_fail (GEON_IS_APPLICATION (self));
  g_return_if_fail (file_uri != NULL);

  GEON_ENTER;

  priv = geon_application_get_instance_private (self);

  parameter = g_variant_new_string (file_uri);

  g_action_group_activate_action (G_ACTION_GROUP (self),
                                  "open-file",
                                  parameter);

  GEON_RETURN;
}

void
geon_application_open_file (GeonApplication *self,
                            GFile           *file)
{
  GeonApplicationPrivate *priv;
  GVariant *parameter;
  gchar *file_uri;

  g_return_if_fail (GEON_IS_APPLICATION (self));
  g_return_if_fail (G_IS_FILE (file));

  GEON_ENTER;

  priv = geon_application_get_instance_private (self);

  file_uri = g_file_get_uri (file);
  parameter = g_variant_new_take_string (file_uri);

  g_action_group_activate_action (G_ACTION_GROUP (self),
                                  "open-file",
                                  parameter);

  GEON_RETURN;
}

void
geon_application_set_accelerator (GeonApplication       *self,
                                  const gchar           *action_name,
                                  const guint            accelerator_key,
                                  const GdkModifierType  accelerator_mods)
{
  GeonApplicationPrivate *priv;
  g_autofree gchar *accelerator_name = NULL;

  g_return_if_fail (GEON_IS_APPLICATION (self));

  GEON_ENTER;

  priv = geon_application_get_instance_private (self);

  accelerator_name = gtk_accelerator_name (accelerator_key,
                                           accelerator_mods);
  const gchar *accelerators[] = {
    accelerator_name,
    NULL
  };

  g_debug ("Mapping action '%s' to accelerator '%s'",
           action_name, accelerator_name);

  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         action_name,
                                         accelerators);

  GEON_RETURN;
}

GMenuModel *
geon_application_get_hamburger_menu (GeonApplication *self)
{
  GeonApplicationPrivate *priv;

  g_return_val_if_fail (GEON_IS_APPLICATION (self), NULL);

  priv = geon_application_get_instance_private (self);

  return geon_application_get_menu (self, "hamburger-menu");
}

const gchar *
geon_application_get_analyzer_name_for_format (GeonFileFormat *format)
{
  GeonApplication *self = GEON_APPLICATION (g_application_get_default ());
  GeonApplicationPrivate *priv;

  g_return_val_if_fail (GEON_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (format != NULL, NULL);

  priv = geon_application_get_instance_private (self);

  return NULL;
}

GeonFileAnalyzer *
geon_application_get_analyzer_for_format (GeonFileFormat *format)
{
  GeonApplication *self = GEON_APPLICATION (g_application_get_default ());
  GeonApplicationPrivate *priv;

  g_return_val_if_fail (GEON_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (format != NULL, NULL);

  priv = geon_application_get_instance_private (self);

  g_assert (priv->analyzers_cache != NULL);

  return g_hash_table_lookup (priv->analyzers_cache,
                              geon_file_format_get_name (format));
}

const gchar *
geon_application_get_loader_name_for_format (GeonFileFormat *format)
{
  GeonApplication *self = GEON_APPLICATION (g_application_get_default ());
  GeonApplicationPrivate *priv;

  g_return_val_if_fail (GEON_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (format != NULL, NULL);

  priv = geon_application_get_instance_private (self);

  return g_hash_table_lookup (priv->loaders_cache,
                              geon_file_format_get_name (format));
}

PeasPluginInfo *
geon_application_get_loader_info_for_format (GeonFileFormat *format)
{
  GeonApplication *self = GEON_APPLICATION (g_application_get_default ());
  GeonApplicationPrivate *priv;
  PeasPluginInfo *plugin_info = NULL;
  const gchar *module_name;

  g_return_val_if_fail (GEON_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (format != NULL, NULL);

  priv = geon_application_get_instance_private (self);

  module_name = g_hash_table_lookup (priv->loaders_cache,
                                     geon_file_format_get_name (format));
  if (module_name != NULL)
    {
      plugin_info = peas_engine_get_plugin_info (priv->loaders_engine,
                                                 module_name);
    }

  return plugin_info;
}

GeonFileLoader *
geon_application_new_loader_for_format (GeonFileFormat *format)
{
  GeonApplication *self = GEON_APPLICATION (g_application_get_default ());
  GeonApplicationPrivate *priv;
  PeasPluginInfo *plugin_info = NULL;
  PeasExtension *extension;
  const gchar *module_name;
  gboolean status;

  g_return_val_if_fail (GEON_IS_APPLICATION (self), NULL);
  g_return_val_if_fail (format != NULL, NULL);

  priv = geon_application_get_instance_private (self);

  g_assert (priv->loaders_engine != NULL);
  g_assert (priv->loaders_cache != NULL);

  module_name = g_hash_table_lookup (priv->loaders_cache,
                                     geon_file_format_get_name (format));
  if (module_name != NULL)
    {
      plugin_info = peas_engine_get_plugin_info (priv->loaders_engine,
                                                 module_name);
    }

  if (plugin_info != NULL)
    {
      if (!peas_plugin_info_is_loaded (plugin_info))
        {
          status = peas_engine_load_plugin (priv->loaders_engine, plugin_info);

          if (status == FALSE)
            {
              g_warning ("Failed loading '%s' loader module",
                        peas_plugin_info_get_module_name (plugin_info));
              return NULL;
            }
        }

      extension = peas_engine_create_extension (priv->loaders_engine,
                                                plugin_info,
                                                GEON_TYPE_FILE_LOADER,
                                                NULL);

      g_assert (GEON_IS_FILE_LOADER (extension));

      return GEON_FILE_LOADER (extension);
    }

  return NULL;
}

/* ex:set ts=2 sw=2 et: */
