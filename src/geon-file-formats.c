/* This file is part of Geon: geon-file-formats.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-FileFormats"

#include "config.h"

#include <glib/gi18n.h>

#include "geon-debug.h"

#include "geon-file-formats.h"

typedef struct
{
  GHashTable *by_name;
  GHashTable *by_file_extension;
  GHashTable *by_content_type;
} GeonFileFormats;

struct _GeonFileFormat
{
  gchar  *familly;
  gchar  *name;

  gchar **file_extensions;
  gchar **content_types;
};

G_DEFINE_BOXED_TYPE (GeonFileFormat, geon_file_format,
                     geon_file_format_copy, geon_file_format_free)

static GeonFileFormats *format_cache = NULL;

GeonFileFormats *
geon_file_formats_new ()
{
  GeonFileFormats *self = g_slice_new0 (GeonFileFormats);

  if (self != NULL)
  {
    self->by_name =
      g_hash_table_new_full (g_str_hash, g_str_equal,
                             NULL, (GDestroyNotify)geon_file_format_free);
    self->by_file_extension =
      g_hash_table_new_full (g_str_hash, g_str_equal,
                             NULL, (GDestroyNotify)g_ptr_array_unref);
    self->by_content_type =
      g_hash_table_new_full (g_str_hash, g_str_equal,
                             NULL, (GDestroyNotify)g_ptr_array_unref);
  }

  return self;
}

void
geon_file_formats_free (gpointer data)
{
  GeonFileFormats *self = (GeonFileFormats *)data;

  if (self != NULL)
    {
      g_clear_pointer (&format_cache->by_content_type, g_hash_table_destroy);
      g_clear_pointer (&format_cache->by_file_extension, g_hash_table_destroy);
      g_clear_pointer (&format_cache->by_name, g_hash_table_destroy);
      g_slice_free (GeonFileFormats, self);
    }
}

GeonFileFormat *
geon_file_format_new (const gchar *familly,
                      const gchar *name)
{
  GeonFileFormat *self;

  g_return_val_if_fail (familly != NULL, NULL);
  g_return_val_if_fail (name != NULL, NULL);

  self = g_slice_new0 (GeonFileFormat);

  if (self != NULL)
    {
      self->familly = g_strdup (familly);
      self->name = g_strdup (name);
    }

  return self;
}

const gchar *
geon_file_format_get_familly (GeonFileFormat *self)
{
  g_return_val_if_fail (self != NULL, NULL);

  return self->familly;
}

const gchar *
geon_file_format_get_name (GeonFileFormat *self)
{
  g_return_val_if_fail (self != NULL, NULL);

  return self->name;
}

void
geon_file_format_add_file_extension (GeonFileFormat *self,
                                     const gchar    *file_extension)
{
  GPtrArray *array;
  guint length, i;

  g_return_if_fail (self != NULL);
  g_return_if_fail (file_extension != NULL);

  if (self->file_extensions == NULL)
    array = g_ptr_array_new_full (2, NULL);
  else
    {
      length = g_strv_length (self->file_extensions);
      array = g_ptr_array_new_full (length + 1, NULL);

      for (i = 0; i < length; i++)
        g_ptr_array_add (array, self->file_extensions[i]);

    g_free (self->file_extensions);
  }

  g_ptr_array_add (array, g_strdup (file_extension));
  g_ptr_array_add (array, NULL);

  self->file_extensions = (gchar **)g_ptr_array_free (array, FALSE);
}

const gchar * const *
geon_file_format_get_file_extensions (GeonFileFormat *self)
{
  g_return_val_if_fail (self != NULL, NULL);

  return (const gchar * const *)self->file_extensions;
}

void
geon_file_format_add_content_type (GeonFileFormat *self,
                                   const gchar    *content_type)
{
  GPtrArray *array;
  guint length, i;

  g_return_if_fail (self != NULL);
  g_return_if_fail (content_type != NULL);

  if (self->content_types == NULL)
    array = g_ptr_array_new_full (2, NULL);
  else
    {
      length = g_strv_length (self->content_types);
      array = g_ptr_array_new_full (length + 1, NULL);

      for (i = 0; i < length; i++)
        g_ptr_array_add (array, self->content_types[i]);

      g_free (self->content_types);
    }

  g_ptr_array_add (array, g_strdup (content_type));
  g_ptr_array_add (array, NULL);

  self->content_types = (gchar **)g_ptr_array_free (array, FALSE);
}

const gchar * const *
geon_file_format_get_content_types (GeonFileFormat *self)
{
  g_return_val_if_fail (self != NULL, NULL);

  return (const gchar * const *)self->content_types;
}

gboolean
geon_file_format_equal (const GeonFileFormat *self,
                        const GeonFileFormat *other)
{
  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (other != NULL, FALSE);

  if (g_strcmp0 (self->name, other->name) == 0)
    return TRUE;

  return FALSE;
}

gboolean
geon_file_format_equiv (const GeonFileFormat *self,
                        const GeonFileFormat *other)
{
  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (other != NULL, FALSE);

  if (g_strcmp0 (self->familly, other->familly) == 0)
    return TRUE;

  return FALSE;
}

gint
geon_file_format_compare (const GeonFileFormat *self,
                          const GeonFileFormat *other)
{
  g_return_val_if_fail (self != NULL, G_MININT);
  g_return_val_if_fail (other != NULL, G_MAXINT);

  return g_strcmp0 (self->name, other->name);
}

GeonFileFormat *
geon_file_format_copy (const GeonFileFormat *self)
{
  GeonFileFormat *copy = NULL;

  if (self != NULL)
    {
      copy = g_slice_dup (GeonFileFormat, self);
      copy->familly = g_strdup (self->familly);
      copy->name = g_strdup (self->name);
      copy->file_extensions = g_strdupv ((gchar **)self->file_extensions);
      copy->content_types = g_strdupv ((gchar **)self->content_types);
    }

  return copy;
}

void geon_file_format_free (GeonFileFormat *self)
{
  if (self != NULL)
    {
      g_clear_pointer (&self->content_types, g_strfreev);
      g_clear_pointer (&self->file_extensions, g_strfreev);
      g_clear_pointer (&self->name, g_free);
      g_clear_pointer (&self->familly, g_free);
      g_slice_free (GeonFileFormat, self);
    }
}

gboolean
geon_file_formats_register (GeonFileFormat *format)
{
  GPtrArray *formats;
  gboolean found;
  guint length, i;

  g_return_val_if_fail (format != NULL, FALSE);

  if (format_cache == NULL)
    format_cache = geon_file_formats_new ();

  g_assert (format_cache != NULL);

  found = g_hash_table_lookup_extended (format_cache->by_name,
                                        format->name,
                                        NULL, NULL);
  if (found == TRUE)
    return TRUE;

  found = g_hash_table_insert (format_cache->by_name,
                               format->name, format);

  g_assert (found == TRUE);

  if (format->file_extensions != NULL)
    {
      length = g_strv_length (format->file_extensions);

      for (i = 0; i < length; i++)
        {
          found = g_hash_table_lookup_extended (format_cache->by_file_extension,
                                                format->file_extensions[i],
                                                NULL, (gpointer)&formats);
          if (found == FALSE)
            {
              formats = g_ptr_array_sized_new (8);

              found = g_hash_table_insert (format_cache->by_file_extension,
                                           format->file_extensions[i], formats);

              g_assert (found == TRUE);
            }

          if (!g_ptr_array_find (formats, format, NULL))
            g_ptr_array_add (formats, format);
        }
    }

  if (format->content_types != NULL)
    {
      length = g_strv_length (format->content_types);

      for (i = 0; i < length; i++)
        {
          found = g_hash_table_lookup_extended (format_cache->by_content_type,
                                                format->content_types[i],
                                                NULL, (gpointer)&formats);
          if (found == FALSE)
            {
              formats = g_ptr_array_sized_new (8);

              found = g_hash_table_insert (format_cache->by_content_type,
                                           format->content_types[i], formats);

              g_assert (found == TRUE);
            }

          if (!g_ptr_array_find (formats, format, NULL))
            g_ptr_array_add (formats, format);
        }
    }

  return TRUE;
}

GeonFileFormat *
geon_file_formats_get_by_name (const gchar *name)
{
  GeonFileFormat *format = NULL;

  g_return_val_if_fail (name != NULL, NULL);

  if (G_LIKELY (format_cache != NULL))
    {
      format = g_hash_table_lookup (format_cache->by_name,
                                    name);
    }

  return format;
}

GList *
geon_file_formats_get_matches (const gchar *content_type,
                               const gchar *file_extension)
{
  GList *matches = NULL;
  GPtrArray *formats;
  guint i;

  g_return_val_if_fail (content_type != NULL || file_extension != NULL, NULL);

  if (G_LIKELY (format_cache != NULL))
    {
      formats = g_hash_table_lookup (format_cache->by_content_type,
                                     content_type);
      if (formats != NULL)
        {
          for (i = 0; i < formats->len; i++)
            {
              GeonFileFormat *format = g_ptr_array_index (formats, i);

              g_assert (format != NULL);

              if (format != NULL)
                matches = g_list_append (matches, format);
            }
        }

      formats = g_hash_table_lookup (format_cache->by_file_extension,
                                     file_extension);
      if (formats != NULL)
        {
          for (i = 0; i < formats->len; i++)
            {
              GeonFileFormat *format = g_ptr_array_index (formats, i);

              g_assert (format != NULL);

              if (format != NULL && g_list_find (matches, format) == NULL)
                matches = g_list_append (matches, format);
            }
        }
    }

  return matches;
}

void
geon_file_formats_foreach (GeonFormatFunc func,
                           gpointer       user_data)
{
  GHashTableIter iter;
  gpointer key, value;

  g_return_if_fail (func != NULL);

  if (G_LIKELY (format_cache != NULL))
    {
      g_hash_table_iter_init (&iter, format_cache->by_name);

      while (g_hash_table_iter_next (&iter, &key, &value))
        {
          GeonFileFormat *format = value;

          if (G_LIKELY (format != NULL))
            (* func) (format, user_data);
        }
    }
}

void
geon_file_formats_flush_out_registrations (void)
{
  if (format_cache != NULL)
    {
      geon_file_formats_free (format_cache);
      format_cache = NULL;
    }
}

/* ex:set ts=2 sw=2 et: */
