/* This file is part of Geon: geon-file-formats.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

#include <glib-object.h>

G_BEGIN_DECLS

#define GEON_TYPE_FILE_FORMAT (geon_file_format_get_type())

typedef struct _GeonFileFormat GeonFileFormat;

GeonFileFormat        *geon_file_format_new                   (const gchar *familly,
                                                               const gchar *name);

const gchar           *geon_file_format_get_familly           (GeonFileFormat *self);

const gchar           *geon_file_format_get_name              (GeonFileFormat *self);

void                   geon_file_format_add_file_extension    (GeonFileFormat *self,
                                                               const gchar    *file_extension);
const gchar   * const *geon_file_format_get_file_extensions   (GeonFileFormat *self);

void                   geon_file_format_add_content_type      (GeonFileFormat *self,
                                                               const gchar    *content_type);
const gchar   * const *geon_file_format_get_content_types     (GeonFileFormat *self);

gboolean               geon_file_format_equal                 (const GeonFileFormat *self,
                                                               const GeonFileFormat *other);

gboolean               geon_file_format_equiv                 (const GeonFileFormat *self,
                                                               const GeonFileFormat *other);

gint                   geon_file_format_compare               (const GeonFileFormat *self,
                                                               const GeonFileFormat *other);

GeonFileFormat        *geon_file_format_copy                  (const GeonFileFormat *self);

void                   geon_file_format_free                  (GeonFileFormat *self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (GeonFileFormat, geon_file_format_free)

gboolean          geon_file_formats_register                  (GeonFileFormat *format);

GeonFileFormat   *geon_file_formats_get_by_name               (const gchar *name);

GList            *geon_file_formats_get_matches               (const gchar *content_type,
                                                               const gchar *file_extension);

typedef void    (*GeonFormatFunc)                             (GeonFileFormat *format,
                                                               gpointer        user_data);

void              geon_file_formats_foreach                   (GeonFormatFunc func,
                                                               gpointer       user_data);

void              geon_file_formats_flush_out_registrations   (void);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
