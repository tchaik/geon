/* This file is part of Geon: geon-tree.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-Tree"

#include "config.h"

#include <glib/gi18n.h>

#include "geon-debug.h"
#include "geon-tree-instance.h"
#include "geon-tree-reference.h"

#include "geon-tree.h"
#include "geon-tree-private.h"

GtkTreeModel *
geon_tree_new (GeonTreeReference *root_reference)
{
  GtkTreeStore *tree;
  const gchar *root_reference_name;
  GtkTreeIter iter;
  GType tree_columns[] = {
    GEON_TYPE_TREE_INSTANCE,
    G_TYPE_STRING,
    GEON_TYPE_TREE_REFERENCE,
    G_TYPE_STRING,
  };

  g_return_val_if_fail (root_reference != NULL, NULL);

  tree =  gtk_tree_store_newv (G_N_ELEMENTS (tree_columns),
                               tree_columns);

  g_assert (tree != NULL);

  root_reference_name = geon_tree_reference_get_name (root_reference);

  gtk_tree_store_insert (tree, &iter, NULL, -1);
  gtk_tree_store_set (tree, &iter,
                      0, NULL, 1, NULL,
                      2, root_reference, 3, root_reference_name, -1);

  /* TODO: Load entire instance-reference tree. */

  return GTK_TREE_MODEL (tree);
}

/* ex:set ts=2 sw=2 et: */
