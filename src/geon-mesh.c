/* This file is part of Geon: geon-mesh.c
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "Geon-Mesh"

#include "config.h"

#include "geon-mesh.h"

struct _GeonMesh
{
  GeonMeshDimension  dimension;
  GeonMeshPrecision  precision;

  guint              element_types: 8;

  GArray            *vertices;
  GArray            *indices;

  gatomicrefcount ref_count;
};

G_DEFINE_BOXED_TYPE (GeonMesh, geon_mesh,
                     geon_mesh_copy, geon_mesh_free)

GeonMesh *
geon_mesh_new (GeonMeshDimension dimension,
               GeonMeshPrecision precision)
{
  GeonMesh *self;

  g_return_val_if_fail (dimension != GEON_MESH_DIMENSION_UNKNOWN, NULL);
  g_return_val_if_fail (precision != GEON_MESH_PRECISION_UNKNOWN, NULL);

  self = g_slice_new0 (GeonMesh);

  g_assert (self != NULL);

  if (self != NULL)
    {
      self->dimension = dimension;
      self->precision = precision;

      if (self->precision == GEON_MESH_PRECISION_DOUBLE)
        self->vertices = g_array_new (FALSE, FALSE, sizeof (gdouble));
      else
        self->vertices = g_array_new (FALSE, FALSE, sizeof (gfloat));

      g_atomic_ref_count_init (&self->ref_count);
    }

  return self;
}

GeonMesh *
geon_mesh_ref (GeonMesh *self)
{
  g_return_val_if_fail (self != NULL, NULL);

  g_atomic_ref_count_inc (&self->ref_count);

  return self;
}

void
geon_mesh_unref (GeonMesh *self)
{
  g_return_if_fail (self != NULL);

  if (g_atomic_ref_count_dec (&self->ref_count))
    geon_mesh_free (self);
}

GeonMeshDimension
geon_mesh_get_dimension (GeonMesh *self)
{
  g_return_val_if_fail (self != NULL, GEON_MESH_DIMENSION_UNKNOWN);

  return self->dimension;
}

GeonMeshPrecision
geon_mesh_get_precision (GeonMesh *self)
{
  g_return_val_if_fail (self != NULL, GEON_MESH_PRECISION_UNKNOWN);

  return self->precision;
}

static guint
geon_mesh_nb_points_for_elements (GeonMeshElementType element_type,
                                  guint               nb_elements)
{
  guint64 nb_vertices = 0;

  g_return_val_if_fail (element_type > GEON_MESH_ELEMENT_TYPE_UNKNOWN, 0);
  g_return_val_if_fail (nb_elements > 0, 0);

  switch (element_type)
    {
    case GEON_MESH_ELEMENT_TYPE_SEGMENT:
      nb_vertices = 2 * nb_elements;
      break;

    case GEON_MESH_ELEMENT_TYPE_TRIANGLE:
      nb_vertices = 3 * nb_elements;
      break;

    case GEON_MESH_ELEMENT_TYPE_QUADRILATERAL:
    case GEON_MESH_ELEMENT_TYPE_TETRAHEDRON:
      nb_vertices = 4 * nb_elements;
      break;

    case GEON_MESH_ELEMENT_TYPE_PYRAMID:
      nb_vertices = 5 * nb_elements;
      break;

    case GEON_MESH_ELEMENT_TYPE_PRISM:
      nb_vertices = 6 * nb_elements;
      break;

    case GEON_MESH_ELEMENT_TYPE_HEXAHEDRON:
      nb_vertices = 8 * nb_elements;
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  if G_LIKELY (nb_vertices <= G_MAXUINT)
    return (guint) nb_vertices;
  else
    return 0;
}

static guint
geon_mesh_array_size_for_elements (GeonMeshDimension   dimension,
                                   GeonMeshElementType element_type,
                                   guint               nb_elements)
{
  guint64 nb_vertices, array_size = 0;

  g_return_val_if_fail (dimension > GEON_MESH_DIMENSION_UNKNOWN, 0);
  g_return_val_if_fail (element_type > GEON_MESH_ELEMENT_TYPE_UNKNOWN, 0);
  g_return_val_if_fail (nb_elements > 0, 0);

  nb_vertices = geon_mesh_nb_points_for_elements (element_type,
                                                  nb_elements);

  if (dimension == GEON_MESH_DIMENSION_2D)
    array_size = 2 * nb_vertices;
  else if (dimension == GEON_MESH_DIMENSION_3D)
    array_size = 3 * nb_vertices;
  else
    g_assert_not_reached ();

  if G_LIKELY (array_size <= G_MAXUINT)
    return (guint) array_size;
  else
    return 0;
}

static gsize
geon_mesh_byte_size_for_elements (GeonMeshDimension   dimension,
                                  GeonMeshPrecision   precision,
                                  GeonMeshElementType element_type,
                                  guint               nb_elements)
{
  guint64 array_size, byte_size = 0;

  g_return_val_if_fail (dimension > GEON_MESH_DIMENSION_UNKNOWN, 0);
  g_return_val_if_fail (precision > GEON_MESH_PRECISION_UNKNOWN, 0);
  g_return_val_if_fail (element_type > GEON_MESH_ELEMENT_TYPE_UNKNOWN, 0);
  g_return_val_if_fail (nb_elements > 0, 0);

  array_size = geon_mesh_array_size_for_elements (dimension,
                                                  element_type,
                                                  nb_elements);

  if (precision == GEON_MESH_PRECISION_SINGLE)
    byte_size = array_size * sizeof (gfloat);
  else if (precision == GEON_MESH_PRECISION_DOUBLE)
    byte_size = array_size * sizeof (gdouble);
  else
    g_assert_not_reached ();

  if G_LIKELY (byte_size <= G_MAXSIZE)
    return (gsize) byte_size;
  else
    return 0;
}

static inline guint
geon_mesh_sum_array_sizes (GArray *array,
                           guint   nb_additional)
{
  if (array == NULL)
    return nb_additional;
  else if G_LIKELY (G_MAXUINT - array->len >= nb_additional)
    return array->len + nb_additional;
  else
    return 0;
}

guint
geon_mesh_allocate_vertices (GeonMesh            *self,
                             GeonMeshElementType  element_type,
                             guint                nb_elements)
{
  guint array_size;

  g_return_val_if_fail (self != NULL, 0);
  g_return_val_if_fail (element_type != GEON_MESH_ELEMENT_TYPE_UNKNOWN, 0);
  g_return_val_if_fail (element_type > 0, 0);

  array_size = geon_mesh_array_size_for_elements (self->dimension,
                                                  element_type,
                                                  nb_elements);
  array_size = geon_mesh_sum_array_sizes (self->vertices,
                                          array_size);

  g_assert (array_size > 0);

  if (self->vertices == NULL)
    {
      if (self->precision == GEON_MESH_PRECISION_DOUBLE)
        self->vertices =
          g_array_sized_new (FALSE, FALSE, array_size, sizeof (gdouble));
      else
        self->vertices =
          g_array_sized_new (FALSE, FALSE, array_size, sizeof (gfloat));
    }
  else
    self->vertices = g_array_set_size (self->vertices, array_size);

  return array_size;
}

guint
geon_mesh_allocate_elements (GeonMesh            *self,
                             GeonMeshElementType  element_type,
                             guint                nb_elements)
{
  guint array_size;

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (element_type != GEON_MESH_ELEMENT_TYPE_UNKNOWN, FALSE);
  g_return_val_if_fail (element_type > 0, FALSE);

  if (!geon_mesh_allocate_vertices (self, element_type, nb_elements))
    return 0;

  array_size = geon_mesh_nb_points_for_elements (element_type,
                                                 nb_elements);
  array_size = geon_mesh_sum_array_sizes (self->indices,
                                          array_size);

  g_assert (array_size > 0);

  if (self->indices == NULL)
    {
      self->indices =
        g_array_sized_new (FALSE, TRUE, array_size, sizeof (guint));
    }
  else
    self->indices = g_array_set_size (self->indices, array_size);

  return (guint) array_size;
}

guint
geon_mesh_add_elements (GeonMesh            *self,
                        GeonMeshElementType  element_type,
                        guint                nb_elements,
                        gconstpointer        vertex_array,
                        gsize                array_size)
{
  guint byte_size;

  g_return_val_if_fail (self != NULL, 0);
  g_return_val_if_fail (element_type != GEON_MESH_ELEMENT_TYPE_UNKNOWN, 0);
  g_return_val_if_fail (nb_elements > 0, 0);
  g_return_val_if_fail (vertex_array != NULL, 0);
  g_return_val_if_fail (array_size > 0, 0);

  byte_size = geon_mesh_byte_size_for_elements (self->dimension,
                                                self->precision,
                                                element_type,
                                                nb_elements);

  g_assert (byte_size == array_size);

  if G_UNLIKELY (byte_size != array_size)
    return 0;

  g_assert (self->vertices != NULL);

  g_array_append_vals (self->vertices, vertex_array, array_size);

  return nb_elements;
}

guint
geon_mesh_add_elements_full (GeonMesh            *self,
                             GeonMeshDimension    dimension,
                             GeonMeshPrecision    precision,
                             GeonMeshElementType  element_type,
                             guint                nb_elements,
                             gconstpointer        vertex_array,
                             gsize                array_size)
{
  gboolean dimension_matches, precision_matches;
  guint byte_size;

  g_return_val_if_fail (self != NULL, 0);
  g_return_val_if_fail (dimension != GEON_MESH_DIMENSION_UNKNOWN, 0);
  g_return_val_if_fail (precision != GEON_MESH_PRECISION_UNKNOWN, 0);
  g_return_val_if_fail (element_type != GEON_MESH_ELEMENT_TYPE_UNKNOWN, 0);
  g_return_val_if_fail (nb_elements > 0, 0);
  g_return_val_if_fail (vertex_array != NULL, 0);
  g_return_val_if_fail (array_size > 0, 0);

  byte_size = geon_mesh_byte_size_for_elements (dimension,
                                                precision,
                                                element_type,
                                                nb_elements);

  g_assert (byte_size == array_size);

  if G_UNLIKELY (byte_size != array_size)
    return 0;

  dimension_matches = self->dimension == dimension;
  precision_matches = self->precision == precision;

  g_assert (self->vertices != NULL);

  if (dimension_matches && precision_matches)
    {
      geon_mesh_add_elements (self,
                              element_type, nb_elements,
                              vertex_array, array_size);
    }
  else
    g_assert_not_reached ();

  return nb_elements;
}

const GArray *
geon_mesh_get_vertex_array (GeonMesh *self)
{
  g_return_val_if_fail (self != NULL, NULL);

  return self->vertices;
}

const GArray *
geon_mesh_get_index_array (GeonMesh *self)
{
  g_return_val_if_fail (self != NULL, NULL);

  return self->indices;
}

GeonMesh *
geon_mesh_copy (const GeonMesh *self)
{
  g_assert_not_reached ();

  return NULL;
}

void
geon_mesh_free (GeonMesh *self)
{
  if (self != NULL)
    {
      g_array_free (self->vertices, TRUE);
      g_array_free (self->indices, TRUE);
      g_slice_free (GeonMesh, self);
    }
}

/* ex:set ts=2 sw=2 et: */
