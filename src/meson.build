libgeon_core_headers = files([
  'geon-debug.h',
  'geon-file-analyzer.h',
  'geon-file-formats.h',
  'geon-file-loader.h',
  'geon-log.h',
  'geon-mesh.h',
  'geon-tree-instance.h',
  'geon-tree-reference.h',
])

libgeon_core_sources = files([
  'geon-file-analyzer.c',
  'geon-file-formats.c',
  'geon-file-loader.c',
  'geon-log.c',
  'geon-mesh.c',
  'geon-tree-instance.c',
  'geon-tree-reference.c',
])

libgeon_core_deps = [
  glib_dep,
  gobject_dep,
  gio_dep,
  graphene_dep,
  peas_dep,
]

libgeon_core_c_args = [
  '-DGEON_COMPILATION',
]

libgeon_core = static_library(
  '@0@-core'.format(package_name),
  libgeon_core_sources,
  dependencies: libgeon_core_deps,
  c_args: libgeon_core_c_args,
  install: false,
)

libgeon_core_dep = declare_dependency(
  link_with: libgeon_core,
  include_directories: include_directories('.'),
  dependencies: libgeon_core_deps,
)

libgeon_modules = []

subdir('analyzers')
subdir('loaders')

libgeon_headers = files([
  'geon-application.h',
  'geon-tree.h',
  'geon-view.h',
  'geon-viewport.h',
  'geon-window.h',
  'geon-world.h',
])

libgeon_sources = files([
  'geon-application.c',
  'geon-tree.c',
  'geon-view.c',
  'geon-viewport.c',
  'geon-window.c',
  'geon-world.c',
])

libgeon_incls = [
  rootdir,
]

libgeon_deps = [
  libgeon_core_dep,
  gtk_dep,
  libadwaita_dep,
  peas_dep,
]

libgeon_c_args = [
  '-DGEON_COMPILATION',
]

subdir('renderers')

libgeon_res = gnome.compile_resources(
  'geon-resources',
  'geon.gresource.xml',
)

libgeon_headers += files('geon.h')

libgeon_sources += [
  libgeon_res.get(0),
]

libgeon_dep_sources = [
  libgeon_res.get(1),
]

install_headers(
  libgeon_core_headers + libgeon_headers,
  install_dir: join_paths(
    pkgincludedir,
    'geon',
  )
)

libgeon = shared_library(
  package_name,
  libgeon_sources,
  version: lib_version,
  include_directories: libgeon_incls,
  dependencies: libgeon_deps,
  c_args: libgeon_c_args,
  link_whole: libgeon_modules,
  install: true,
  install_dir: join_paths(
    libdir,
    package_string
  )
)

if generate_gir == true
  libgeon_gir_includes = [
    'GLib-2.0',
    'GObject-2.0',
    'Gio-2.0',
    'Gtk-4.0',
  ]

  libgeon_gir_extra_args = [
    '-DGEON_COMPILATION',
  ]

  if gtk_is_subproject
    libgeon_gir_extra_args += [
      '--add-include-path', gtk_subproject_typelib_dir]
  endif

  libgeon_gir = gnome.generate_gir(
    libgeon,
    sources: libgeon_sources + libgeon_headers,
    nsversion: api_version,
    namespace: 'Geon',
    symbol_prefix: 'geon',
    identifier_prefix: 'Geon',
    export_packages: [package_string],
    includes: libgeon_gir_includes,
    header: 'geon/geon.h',
    link_with: libgeon,
    extra_args: libgeon_gir_extra_args,
    install: true,
    install_dir_gir: girdir,
    install_dir_typelib: typelibdir,
  )

  libgeon_dep_sources += [
    libgeon_gir,
  ]

  if generate_vapi == true
    libgeon_vapi_packages = [
      'glib-2.0',
      'gobject-2.0',
      'gio-2.0',
      'gtk-4.0',
    ]

    libgeon_vapi = gnome.generate_vapi(
      package_string,
      sources: libgeon_gir.get(0),
      packages: libgeon_vapi_packages,
      install: true,
      install_dir: vapidir,
    )
  endif
endif

libgeon_dep = declare_dependency(
  link_with: libgeon,
  include_directories: include_directories('.'),
  sources: libgeon_dep_sources,
  dependencies: libgeon_deps,
)

geon_sources = [
  'geon.c',
]

geon_deps = [
  libgeon_dep,
]

geon_c_args = [
  '-DGEON_COMPILATION',
]

geon_exe = executable(
  package_name,
  geon_sources,
  dependencies: geon_deps,
  c_args: geon_c_args,
  install: true,
  install_dir: bindir,
  install_rpath: join_paths(
    libdir,
    package_string
  )
)
