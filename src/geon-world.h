/* This file is part of Geon: geon-world.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

#include <gio/gio.h>

G_BEGIN_DECLS

#define GEON_TYPE_WORLD (geon_world_get_type())

G_DECLARE_FINAL_TYPE (GeonWorld, geon_world,
                      GEON, WORLD,
                      GObject)

GeonWorld      *geon_world_new_for_file                 (GFile                *file,
                                                         GError              **error);
void            geon_world_new_for_file_async           (GFile                *file,
                                                         GCancellable         *cancellable,
                                                         GAsyncReadyCallback   callback,
                                                         gpointer              user_data);
GeonWorld      *geon_world_new_for_file_finish          (GAsyncResult         *result,
                                                         GError              **error);

GeonWorld      *geon_world_new_for_uri                  (const gchar          *uri,
                                                         GError              **error);
void            geon_world_new_for_uri_async            (const gchar          *uri,
                                                         GCancellable         *cancellable,
                                                         GAsyncReadyCallback   callback,
                                                         gpointer              user_data);
GeonWorld      *geon_world_new_for_uri_finish           (GAsyncResult         *result,
                                                         GError              **error);

gboolean        geon_world_load_model_tree              (GeonWorld            *self,
                                                         GCancellable         *cancellable,
                                                         GError              **error);
void            geon_world_load_model_tree_async        (GeonWorld            *self,
                                                         GCancellable         *cancellable,
                                                         GAsyncReadyCallback   callback,
                                                         gpointer              user_data);
gboolean        geon_world_load_model_tree_finish       (GeonWorld            *self,
                                                         GAsyncResult         *result,
                                                         GError              **error );

gboolean        geon_world_load_model_geometry          (GeonWorld            *self,
                                                         GCancellable         *cancellable,
                                                         GError              **error);
void            geon_world_load_model_geometry_async    (GeonWorld            *self,
                                                         GCancellable         *cancellable,
                                                         GAsyncReadyCallback   callback,
                                                         gpointer              user_data);
gboolean        geon_world_load_model_geometry_finish   (GeonWorld            *self,
                                                         GAsyncResult         *result,
                                                         GError              **error);

GFile          *geon_world_get_file                      (GeonWorld *self);

gboolean        geon_world_is_assembly                   (GeonWorld *self);

GtkTreeModel   *geon_world_get_root_tree                 (GeonWorld *self);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
