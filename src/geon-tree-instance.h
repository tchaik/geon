/* This file is part of Geon: geon-tree-instance.h
 *
 * Copyright © 2018 Martin Blanchard <tchaik@gmx.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#if !defined(__GEON_INSIDE__) && !defined(GEON_COMPILATION)
# error "Only <geon/geon.h> can be included directly."
#endif

#include <glib-object.h>

#include "geon-tree-reference.h"

G_BEGIN_DECLS

#define GEON_TYPE_TREE_INSTANCE (geon_tree_instance_get_type())

G_DECLARE_FINAL_TYPE (GeonTreeInstance, geon_tree_instance,
                      GEON, TREE_INSTANCE,
                      GObject)

GeonTreeInstance    *geon_tree_instance_new             (const gchar       *name,
                                                         GeonTreeReference *reference);

const gchar         *geon_tree_instance_get_name        (GeonTreeInstance *self);

void                 geon_tree_instance_set_name        (GeonTreeInstance *self,
                                                         const gchar      *name);

GeonTreeReference   *geon_tree_instance_get_reference   (GeonTreeInstance *self);

G_END_DECLS

/* ex:set ts=2 sw=2 et: */
